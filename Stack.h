/*
Name:Stack.h
Copyright:
Author:Amit Malyala
Description:
*/
#ifndef STACK_H
#define STACK_H

#include "std_types.h"
#include "Lexer.h"
#include <iostream>
#include <vector>
#include <stack>
#include "Tree.h"
#include "Error.h"

//#define STACK_DEBUGGING_ENABLED 1


/* 10000 operands and operators are enough for arithmetic processor with less than 100 expressions
   If there are stack access errors, increase number of operators and operands. */
#define MAX 10000
/*
Component Function: BOOL PopNode(Tnode *&aNode)
Arguments:  Reference to node being popped
Returns: true or not
Description:  Returns true if pop is successful
Version : 0.1
 */
BOOL PopNode(Tnode *&aNode);

/*
Component Function: BOOL PushNode(Tnode &aNode);
Arguments:  Reference to Tree node being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushNode(Tnode &aNode);
/*
Component Function: void InitTokenStack(void);
Arguments:  None
Returns: None
Description:  Initializes Token stack
Version: 0.1 Initial version
 */
void InitTokenStack(void);

/*
Component Function: void ClearTokenStack(void)
Arguments:  None
Returns: None
Description:  Clears Token Stack.
Version : 0.1
 */
void ClearTokenStack(void);

/*
Component Function: BOOL isTokenStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isTokenStackEmpty(void);


/*
Component Function: BOOL PushTokenStack(const Lexer::Token_tag& px);
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenStack(const Lexer::Token_tag& px);


/*
Component Function: void ReverseTokenStack(void)
Arguments:  None
Returns: Nothing
Description:  Reverses  Tokenstack
Version : 0.1
 */
void ReverseTokenStack(void);
/*
Component function: BOOL PopTokenStack(Token_tag& px);
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenStack(Lexer::Token_tag& px);

/*
Component function: Lexer::Token_tag GetTopinTokeneStack(void);
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
Lexer::Token_tag GetTopinTokeneStack(void);

/*
Component Function: void InitStack(void)
Arguments:  None
Returns: None
Description:  Initializes Operator and RPN stack
Version: 0.1 Initial version
*/
void InitStack(void);
/* Definitions for composite data in stack which contains operands and operators for
   RPN stack*/


/*
Component Function: BOOL PopTokenOpStack(Lexer::Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenOpStack(Lexer::Token_tag& px);

/*
Component Function: BOOL isOpStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isOpStackEmpty(void);


/*
Component Function: BOOL isArithmeticEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isArithmeticStackEmpty(void);

/*
Component Function: BOOL isPostfixStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isPostfixStackEmpty(void);



/*
Component Function: BOOL PopTokenPostfixStack(Lexer::Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenPostfixStack(Lexer::Token_tag& px);

/*
Component Function: BOOL PushTokenPostfixStack(const Lexer::Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenPostfixStack(const Lexer::Token_tag& Token);
/*
Component Function: BOOL PushToken(const Lexer::Token_tag& Token);
Arguments:  Reference to stack, data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenOpStack(const Lexer::Token_tag& Token);
/*
Component Function: void DisplayPostfixStack(const std::vector <Lexer::Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  Displays Postfix stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void DisplayPostfixStack(const std::vector <Lexer::Token_tag> &pS);
/*
Component Function: BOOL PopTokenArithmeticStack(Lexer::Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenArithmeticStack(Lexer::Token_tag& px);

/*
Component Function: BOOL PushTokenArithmeticStack(const Lexer::Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenArithmeticStack(const Lexer::Token_tag& Token);

/*
Component Function: void DisplayOpStack(const std::vector <Lexer::Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:
Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void DisplayOpStack(const std::vector <Lexer::Token_tag> &pS);

/*
Component Function: void ReversePostfixStack(void)
Arguments:  None
Returns: Nothing
Description:  Reverses a postfix stack in RPN notation.
Version : 0.1
 */
void ReversePostfixStack(void);





/*
Component Function: BOOL isTreeStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isTreeStackEmpty(void);


/*
Component Function: BOOL isTreeStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if full
Version : 0.1
 */
BOOL isTreeStackFull(void);




/*
Component Function: void PrintTreeStack(void)
Arguments:  None
Returns: None
Description:  Displays Tree stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void PrintTreeStack(void);

/*
Component Function: void PrintTreeStackPos(void)
Arguments:  None
Returns: None
Description:  Displays Tree stack top pos
Version : 0.1
Notes:
This function would be used while construction of AST.
*/
void PrintTreeStackPos(void);


/*
Component Function: void PrintOpStack(void)
Arguments:  None
Returns: None
Description:  Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void PrintOpStack(void);
/*
Component Function: void PrintPostfixStack(void)
Arguments:  None
Returns: None
Description:  Displays Postfix stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
*/
void PrintPostfixStack(void);



/*
Component Function: void PrintOpStack(void)
Arguments:  None
Returns: None
Description:  Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void PrintOpStack(void);

/*
Component Function: BOOL PushTokenOpStack(const Lexer::Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenOpStackVector(const Lexer::Token_tag& Token);

/*
Component Function: BOOL PopOpStackVector (Lexer::Token_tag& Token)
Arguments: Op Token being poppped
Returns: true or not
Description:  Returns true if push is successful
Version : 0.1
*/
BOOL PopOpStackVector(Lexer::Token_tag& Token);

/*
Component Function: Tnode* GetTopNodeinTreeStack(void)
Arguments: Top node in a stack
Returns: Pointer to top most node.
Description:  Returns true if push is successful
Version : 0.1
*/
Tnode* GetTopNodeinTreeStack(void);

#endif // #ifndef STACK_H
