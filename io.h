
/*
Name : Io.h
Author: Amit Malyala
Description:
Read a input ascii file into a string.
Version: 0.1
*/

#ifndef INPUT_H
#define INPUT_H

#include <string>
#include <fstream>
#include <iostream>
#include "std_types.h"
#include "Error.h"

namespace io
{
/*
Another solution:
This function would be called by getTokencharString() function;
*/
SCHAR8* getInput(void);

}

#endif