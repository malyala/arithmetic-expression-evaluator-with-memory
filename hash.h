#ifndef HASH
#define HASH
#include <cstddef>


/* Use of #define constants is deprecated in C++14/C++17 */
/* Define size of each bucket list. */
/* This size would be used for hash table for constructing scopes with their names*/

constexpr std::size_t MINIHASHTABLESIZE = 53; /* This can be adjusted according to memory availability*/

/*
 Component Function: inline std::size_t StringHash(char const* str)
 Arguments:  String to be hashed
 Description: This function converts a char string into  a number and returns it as unsigned int.
 Returns: string hash as uint.
 Version : 0.1 Initial version with XOR
		   0.2 Added shift of mask,char and weight
		   0.3 Reduced a few arithmetic operations.
		   0.4 Removed index variable. Removed function call to strlength()
 Notes:
 */
inline std::size_t StringHash(char const* str)
{
    std::size_t mask = 255;
    std::size_t weight = 3;
    std::size_t Hashvalue = 0;
    while (*str)
    {
        /* This seems to have good distribution of hash values for different strings.*/
        Hashvalue += (((mask << 3) ^ (*str)) + ((*str) >> 1) + weight);
        str++;
    }
    return (Hashvalue % (MINIHASHTABLESIZE));
}



#endif
