
/*
Name: Stack.cpp
Copyright: Amit Malyala, 2016. All rights reserved.
Author: Amit Malyala
Date: 30-06-19
Version:
0.01 Initial version

Reference:
Foundations of computer science - Ullman and Aho

Description:
Stack implementation using a vector
To do:

Notes:
*/

#include "stack.h"
static  std::vector <Lexer::Token_tag> TokenStack;
/*
Stacks for Operands and operators which have items of type Token_tag.
*/
/* A stack for Tnode type data while constructing binary tree*/
std::stack <Tnode > TreeStack;

static std::vector<Lexer::Token_tag> OpStack;

static std::stack <Lexer::Token_tag> ArithmeticStack;

static std::vector<Lexer::Token_tag> PostfixStack;

/*
Component Function: static BOOL isPostfixStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isPostfixStackFull(void);


/*
Component Function: static BOOL isArithmeticStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isArithmeticStackFull(void);



/*
Component Function: static void ClearTreeStack(TStack& pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearTreeStack(std::stack <Tnode> & pS);



/*
Component Function: static BOOL isOpStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isOpStackFull(void);


/*
Component Function: static void ClearStack(std::vector <Lexer::Token_tag> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
*/
static void ClearStack(std::vector <Lexer::Token_tag> & pS);

/*
Component Function: static void ClearArithmeticStack(std::stack <Lexer::Token_tag> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearArithmeticStack(std::stack <Lexer::Token_tag> & pS);



/* Comment to integrate with the project, UnComment to test */
#if(0)
SINT32 main(void)
{
    /* General Init functions for stacks */
    InitStack();
    if (CheckforErrors())
    {
        PrintErrors();
    }
    return 0;
}
#endif

/*
Component Function: void InitStack(void)
Arguments:  None
Returns: None
Description:  Initializes Operator and RPN stack
Version: 0.1 Initial version
 */
void InitStack(void)
{
    ClearStack(PostfixStack);
    ClearStack(OpStack);
    ClearTokenStack();
    ClearArithmeticStack(ArithmeticStack);

    ClearTreeStack(TreeStack);


}

/* Function Definitions for composite data in a stack which contains operands and operators for
   RPN stack*/

/*
Component Function: static void ClearStack(std::vector <Lexer::Token_tag> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
*/
static void ClearStack(std::vector <Lexer::Token_tag> & pS)
{
    pS.clear();
}


/*
Component Function: static void ClearTreeStack(TStack& pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearTreeStack(std::stack <Tnode> & pS)
{
    while(pS.size())
    {
        pS.pop();
    }
}



/*
Component Function: BOOL isArithmeticEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isArithmeticStackEmpty(void)
{
    return (ArithmeticStack.empty());
}

/*
Component Function: static BOOL isArithmeticEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isArithmeticStackFull(void)
{
    std::size_t size= ArithmeticStack.size();
    std::size_t ReturnValue = false;
    if ( size == MAX)
    {
        ReturnValue = true;
    }
    else
    {
        ReturnValue =false;
    }
    return ReturnValue;
}


/*
Component Function: BOOL PopTokenPostfixStack(Lexer::Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenPostfixStack(Lexer::Token_tag& px)
{
    BOOL ReturnValue = false;
    if (isPostfixStackEmpty())
    {

        ReturnValue = false;
    }
    else
    {
        px = PostfixStack.back();
        PostfixStack.pop_back();
        ReturnValue = true;
    }
    /*
      #if defined (STACK_DEBUGGING_ENABLED)
        //std::cout << "\nPos is at " << PostfixStack.Pos << std::endl;
    #endif
    */
    return ReturnValue;
}

/*
Component Function: BOOL PushTokenPostfixStack(const Lexer::Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenPostfixStack(const Lexer::Token_tag& Token)
{
    BOOL ReturnValue = false;
    if (isPostfixStackFull())
    {
        ErrorTracer(STACK_ACCESS_ERROR, Token.LineNumber, Token.ColumnNumber);
        ReturnValue = false;
    }
    else
    {
        PostfixStack.push_back(Token);
        ReturnValue = true;
    }
    return ReturnValue;
}


/*
Component Function: BOOL PopTokenArithmeticStack(Lexer::Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenArithmeticStack(Lexer::Token_tag& px)
{
    BOOL ReturnValue = false;
    if (isArithmeticStackEmpty())
    {
        ReturnValue = false;
    }
    else
    {
        px = ArithmeticStack.top();
        ArithmeticStack.pop();
        ReturnValue = true;
    }
    /*
      #if defined (STACK_DEBUGGING_ENABLED)
        //std::cout << "\nPos is at " << PostfixStack.Pos << std::endl;
    #endif
    */
    return ReturnValue;
}

/*
Component Function: BOOL PushTokenArithmeticStack(const Lexer::Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenArithmeticStack(const Lexer::Token_tag& Token)
{
    BOOL ReturnValue = false;
    if (isArithmeticStackFull())
    {
        ErrorTracer(STACK_ACCESS_ERROR, Token.LineNumber, Token.ColumnNumber);
        ReturnValue = false;
    }
    else
    {
        ArithmeticStack.push(Token);
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: static void ClearArithmeticStack(std::stack <Lexer::Token_tag> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearArithmeticStack(std::stack <Lexer::Token_tag> & pS)
{
    while(pS.size())
    {
        pS.pop();
    }
}

/*
Component Function: BOOL PopTokenOpStack(Lexer::Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenOpStack( Lexer::Token_tag& px)
{
    BOOL ReturnValue = false;
    if (isOpStackEmpty())
    {

        ReturnValue = false;
    }
    else
    {
        px = OpStack.back();
        OpStack.pop_back();
        ReturnValue = true;
    }
    /*
      #if defined (STACK_DEBUGGING_ENABLED)
        //std::cout << "\nPos is at " << pS.Pos << std::endl;
    #endif
    */
    return ReturnValue;
}


/*
Component Function: static BOOL isOpStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isOpStackFull(void)
{
    std::size_t size= OpStack.size();
    BOOL ReturnValue=false;
    if ( size == 100)
    {
        ReturnValue=true;
    }
    return  ReturnValue;
}

/*
Component Function: BOOL PushTokenOpStack(const Lexer::Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenOpStack(const Lexer::Token_tag& Token)
{
    BOOL ReturnValue = false;
    if (isOpStackFull())
    {
        ErrorTracer(STACK_ACCESS_ERROR, Token.LineNumber, Token.ColumnNumber);
        ReturnValue = false;
    }
    else
    {
        OpStack.push_back(Token);
        ReturnValue = true;
    }
    return ReturnValue;
}



/*
Component Function: BOOL isOpStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isOpStackEmpty(void)
{
    return  OpStack.empty();
}



/*
Component Function: BOOL isPostfixStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isPostfixStackEmpty(void)
{
    return PostfixStack.empty();
}


/*
Component Function: static BOOL isPostfixStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isPostfixStackFull(void)
{
    std::size_t size = PostfixStack.size();
    BOOL ReturnValue = false;
    if ( size == MAX)
    {
        ReturnValue=true;
    }
    return ReturnValue;
}

/*
Component Function: void PrintOpStack(void)
Arguments:  None
Returns: None
Description:  Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void PrintOpStack(void)
{
    DisplayOpStack(OpStack);
}



/*
Component Function: void DisplayOpStack(const std::vector <Lexer::Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:
Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void DisplayOpStack(const std::vector <Lexer::Token_tag> &pS)
{
    //std::cout << "\nOperator stack is:";
    SINT32 index = 0;
    if (!(isOpStackEmpty()))
    {
        std::cout << "Printing Op stack" << std::endl;
        for (index = pS.size()-1; index >= 0; index--)
        {
            if (pS[index].Type == Lexer::TokenType::separator_leftParen)
            {
                std::cout << "\nLeft Parenthesis " << pS[index].Data.u.cString;
            }
            else if (pS[index].Type == Lexer::TokenType::op)
            {
                std::cout << pS[index].Data.u.cString;
            }
        }
    }
    else
    {
        std::cout << "\nOperatorStack empty";

    }
}



/*
Component Function: void PrintPostfixStack(void)
Arguments:  None
Returns: None
Description:  Displays Postfix stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void PrintPostfixStack(void)
{
    //std::cout << "in PrintPostfixStack" << std::endl;
    DisplayPostfixStack(PostfixStack);
}


/*
Component Function: void DisplayPostfixStack(const std::vector <Lexer::Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  Displays Postfix stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void DisplayPostfixStack(const std::vector <Lexer::Token_tag> &pS)
{
    std::cout << "Postfix stack is:" << std::endl;
    SINT32 index = 0;
    if (!(isPostfixStackEmpty()))
    {
        for (index = pS.size()-1; index >= 0 ; index--)
        {
            Lexer::PrintToken(pS[index]);
        }
    }
    else
    {
        std::cout << "Output stack empty" << std::endl;

    }
}


/*
Component Function: void ReversePostfixStack(void)
Arguments:  None
Returns: Nothing
Description:  Reverses a postfix stack in RPN notation.
Version : 0.1
*/
void ReversePostfixStack(void)
{
    SINT32 index=0;
    Lexer::Token_tag Temp;
    SINT32 j=0;
    for(j=0,index=(PostfixStack.size()-1); j<index; --index,++j)
    {
        Temp = PostfixStack[index];
        PostfixStack[index]=PostfixStack[j];
        PostfixStack[j]= Temp;
    }
}
/*
Component Function: void PrintTreeStackPos(void)
Arguments:  None
Returns: None
Description:  Displays Tree stack top pos
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void PrintTreeStackPos(void)
{
    //std::cout << "TreeStack top pos at " << TreeStack.Pos << std::endl;
}


/*
Component Function: BOOL isTreeStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isTreeStackEmpty(void)
{
    BOOL ReturnValue = false;

    if (TreeStack.empty())
    {
        ReturnValue = true;
    }
    else
    {
        ReturnValue = false;
    }
    return ReturnValue;
}

/*
Component Function: BOOL isTreeStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if full
Version : 0.1
 */
BOOL isTreeStackFull(void)
{
    BOOL ReturnValue = false;
    std::size_t size = TreeStack.size();
    if (size == MAX)
    {
        ReturnValue = true;
    }
    else
    {
        ReturnValue = false;
    }
    return ReturnValue;
}





/*
Component Function: void InitTokenStack(void)
Arguments:  None
Returns: None
Description:  Initializes Token stack
Version: 0.1 Initial version
 */
void InitTokenStack(void)
{
    ClearTokenStack();
}

/*
Component Function: void ClearTokenStack(void)
Arguments:  None
Returns: None
Description:  Clears Token Stack.
Version : 0.1
 */
void ClearTokenStack(void)
{
    TokenStack.clear();
}
/*
Component Function: void ReverseTokenStack(void)
Arguments:  None
Returns: Nothing
Description:  Reverses  Tokenstack
Version : 0.1
 */
void ReverseTokenStack(void)
{
    SINT32 index=0;
    Lexer::Token_tag Temp;
    SINT32 j=0;
    for(j=0,index=(TokenStack.size()-1); j<index; --index,++j)
    {
        Temp = TokenStack[index];
        TokenStack[index]=TokenStack[j];
        TokenStack[j]= Temp;
    }

}
/*
Component Function: BOOL isToeknStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isTokenStackEmpty(void)
{
    return TokenStack.empty();
}


/*
Component Function: BOOL PopTokenStack(Lexer::Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenStack(Lexer::Token_tag& px)
{
    BOOL ReturnValue = false;
    if (TokenStack.empty())
    {
        ReturnValue = false;
    }
    else
    {
        px = TokenStack.back();
        TokenStack.pop_back();
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: BOOL PushTokenStack(const Lexer::Token_tag& px)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenStack(const Lexer::Token_tag& px)
{
    BOOL ReturnValue = false;
    if (TokenStack.size()==9999100)
    {
        ErrorTracer(STACK_ACCESS_ERROR, 0,0);
        ReturnValue = false;
    }
    else
    {
        TokenStack.push_back(px);
        ReturnValue = true;
    }
    return ReturnValue;
}



/*
Component BOOL PushNode(Tnode& aNode)
Arguments: Tree node being pushed
Returns: true or not
Description:  Returns true if push is successful
Version : 0.1
 */
BOOL PushNode(Tnode& aNode)
{
    BOOL ReturnValue = false;
    if (isTreeStackFull())
    {
        ErrorTracer(STACK_ACCESS_ERROR, aNode.ColumnNumber, aNode.ColumnNumber);
        ReturnValue = false;
    }
    else
    {
        TreeStack.push(aNode);

        //std::cout <<"Node being pushed: " << std::endl;
        //PrintTreeNode(aNode);
        ReturnValue = true;
    }
    return ReturnValue;
}


/*
Component Function: BOOL PopNode(Tnode *&aNode)
Arguments: Tree node being pushed
Returns: true or not
Description:  Returns true if push is successful
Version : 0.1
*/
BOOL PopNode(Tnode *&aNode)
{
    BOOL ReturnValue = false;
    if (isTreeStackEmpty())
    {
        ReturnValue = false;
    }
    else
    {
        *aNode = TreeStack.top();
        TreeStack.pop();
        //std::cout <<"Node being popped: " << std::endl;
        //PrintTreeNode(aNode);

        ReturnValue = true;
    }
    return ReturnValue;
}



/*
Component Function: Tnode* GetTopNodeinTreeStack(void)
Arguments: Top node in a stack
Returns: Pointer to top most node.
Description:  Returns true if push is successful
Version : 0.1
*/
Tnode* GetTopNodeinTreeStack(void)
{
    Tnode *anode=nullptr;
    if (!isTreeStackEmpty())
    {
        anode = & TreeStack.top();
        TreeStack.pop();
        //std::cout <<"Node being popped: " << std::endl;
        //PrintTreeNode(aNode);
    }
    return anode;
}
