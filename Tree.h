/*
Name :  Tree.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 05-10-2016
Description:
This module provides tree node definitions
 */


// Definition of a Tree node
#ifndef TREE_H
#define TREE_H



#include "std_types.h"
#include <vector>

#include "Evaluator.h"
#include <iostream>
#include "Evaluator.h"
#include "Error.h"
#include "SymbolTable.h"
#include <cmath>




/* If development debugging enabled */
/*
#define TREE_DEBUGGING_ENABLED 0x01
*/

/* Tree node for construction of expression tree */
class Tnode
{
public:
    Lexer::Token_tag Token;
    std::size_t LineNumber, ColumnNumber;
    Tnode *LeftChild, *RightChild, *pParent;
    /* constructor */
    Tnode();
    /* destructor */
    ~Tnode();
};



#endif /* #ifndef TREE_H */
