/*Name :  Lexer.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module creates lexer tokens.
Notes:




Notes:
Create Ops <<=   >>=

in a statement
a= 1

token a is identified as ident and added into symbol table.Each ident to the left of '=' is added into symbol
idents on the right of '=' are looked up.
The lexer looks up symbol names. The parser initializes symbols/idents durng construction of syntax tree.

To do:
Recognize operators as
-+
+-
/-
*-
as separate ops

Bug and revision history;
0.01 Initial version
0.02 Changed getNextToken and TokenProcessor() functions
*/
#include "Lexer.h"
#include "Stack.h"
#include <cstring>
// Comment when module is integrated with the project
// Uncomment to develop or test

/* Declare external scope */
extern Scopes* CurrentScope;

#if(0)
SINT32 main(void)
{
    /* First try with stack, then a linked list */
    InitTokenStack();
    Lexer::Token_tag Token;
    while(1)
    {
        Token= Lexer::getNextToken();
    }
    return 0;
}
#endif


/*
Component function: SCHAR8 Lexer::PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index)
Arguments:  char string, length, current index.
Returns: next char of index.
Description:  returns a char
Version : 0.1
*/
SCHAR8 Lexer::PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index)
{
    //std::cout << "in peek next char function " << std::endl;
    if ((index+1)<Length)
    {
        return str[index+1];
    }
    else
    {
        return ENDOFINPUTSTRING;
    }
}


/*
 Component Function: BOOL Lexer::IsWhiteSpace(SCHAR8 Character)
 Arguments:  Character
 Returns:
 Description:
 This function checks if a character is whitespace or not
  Version : 0.1
 */
BOOL Lexer::isWhitespace(SCHAR8 Character)
{
    return (Character == ' ' || Character == '\t' );
}


/* Skip whitespace */
void Lexer::SkipWhitespace(SCHAR8* str, std::size_t Length, std::size_t& i)
{
    SCHAR8 ch; /* Use this variable outside of function?*/
    do
    {
        ch = PeekNextChar(str,Length,i);
        if (Lexer::isWhitespace(ch))
        {
            i++;
        }
    }
    while (Lexer::isWhitespace(ch));
}

/*
 Component Function: BOOL Lexer::isIdentChar(SCHAR8 Character)
 Arguments:  Character to be searched.
 Returns:
 Description:
 Returns is ident char or not.
  Version : 0.1
 */
BOOL Lexer::isIdentChar(SCHAR8 Character)
{
    return (( DataStructures::isAlphabet(Character)) || Character == '_' || Character == '.'|| (DataStructures::isDigit(Character) == true));
}

/*
 Component Function: BOOL Lexer::isSeparator(SCHAR8 Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL Lexer::isSeparator(SCHAR8 Character)
{
    return (Character == ')' || Character == '(');
}

/*
 Component Function: BOOL Lexer::isCommand(const SCHAR8* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function checks if char string is a command
 Returns ident or not
 Version : 0.1
 */
BOOL Lexer::isCommand(const SCHAR8* str)
{
	BOOL match=false;
	if (DataStructures::isAlpha(str))
	{
		SCHAR8* Temp=  new SCHAR8[DataStructures::strlength(str)+1];
	    Memcopy(Temp,str);
 	    DataStructures::tolower(Temp);
 	    
	    if ((strcmp(Temp,"clear") ==0 ) || (strcmp(Temp,"exit") ==0) || (strcmp(Temp,"quit") ==0) )
	    {
           match=true;
	    } 
	    delete[] Temp;
	}
	return match;
}
/*
 Component Function: BOOL Lexer::isIdent(const SCHAR8* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z and digits
 Returns ident or not
 Version : 0.1
 */
BOOL Lexer::isIdent(const SCHAR8* str)
{
    std::size_t i = 0;
    std::size_t StrLength = DataStructures::strlength(str);
    std::size_t CharCount = 0;
    BOOL isIdent = false;
    std::size_t Alphabetcount=0;
    while (i < StrLength)
    {
        if (isIdentChar(str[i]))
        {
            if (DataStructures::isAlphabet(str[i]))
            {
                Alphabetcount++;
            }
            CharCount++;
        }
        i++;
    }
    if ((CharCount == StrLength) && (StrLength >0) && (Alphabetcount))
    {
        if (DataStructures::isAlphabet(str[0] ) || str[0] == '_')
        {
            isIdent = true;
        }
        else
        {
            ErrorTracer(IDENTIFIER_STARTCHAR_ERROR,0,0);
        }
    }
    return isIdent;
}

/*
 Component Function: BOOL Lexer::isOperator(const SCHAR8* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function checks if char string is a operator.
 Returns op or not
 Version : 0.1
 */
BOOL Lexer::isOperator(const SCHAR8* str)
{
    std::size_t i = 0;
    std::size_t StrLength = DataStructures::strlength(str);
    std::size_t CharCount = 0;
    BOOL isOp = false;
    while (i < StrLength)
    {
        if (Lexer::isOperchar(str[i]))
            CharCount++;
        i++;
    }

    if (CharCount == StrLength)
    {
        isOp = true;
    }
    return isOp;
}
/*
 Component Function: BOOL Lexer::isOperchar(SCHAR8 Ch)
 Arguments:  character
 Returns:
 Description:
 returns char is a operator or not
  Version : 0.1
 */
BOOL Lexer::isOperchar(SCHAR8 Ch)
{
    BOOL isoperchar =false;
    switch (Ch)
    {
    case '+':
    case '-':
    case '/':
    case '*':
    case '=':
    case '<':
    case '>':
    case '|':
    case '&':
    case '~':
    case '!':
    case '^':
        isoperchar=true;
        break;
    default:
        break;
    }
    return isoperchar;
}


/*
Component function: Lexer::Token_tag Lexer::getNextToken(void)
Arguments:  None
Returns: Token
Description:  returns a token
Notes:
This function gets each char string from each line entered and detects invidual tokens in it.
Each time this function is called, it returns a token. When all of the char string are scanned,
it gets a new char string and the process is repeated.If there are incomplete tokens or errors,
a error is signalled and the the program resumes functioning.
Version : 0.1
*/

Lexer::Token_tag Lexer::getNextToken(void)
{
    Lexer::Token_tag Token;
    /* check each token string, classify it as ident, operator, parenthesis or numeric value.*/
    SCHAR8* Tokenstring= getTokencharstring();
    Token = Lexer::TokenProcessor(Tokenstring);
    /* Return token */
    return Token;
}


/*
 Component function: Lexer::Token_tag Lexer::TokenProcessor(SCHAR8* Tokenstring)
 Arguments:  char string
 returns : Token
 Version : 0.1
 Description:
 Returns current Token after classifying it from a stream of tokens created on the fly.
 Fetches next token.
 Each token can be these:
 ident,
 numeric,
 operator
 Separator- parenthesis ( ) or ' '
 Newline \n

 Notes:
 Known issues:

 */
Lexer::Token_tag Lexer::TokenProcessor(SCHAR8* Tokenstring)
{

    /* check each token string, classify it as ident, operator, parenthesis or numeric value.*/
    Lexer::Token_tag Token;
    static std::size_t LeftParenCount=0;
    static std::size_t RightParenCount=0;
    
    if (strcmp(Tokenstring,"\n") ==0)
    {
        if ((LeftParenCount>RightParenCount) || (LeftParenCount <RightParenCount))
        {
            ErrorTracer(MISSING_PARENTHESES,0,0);
        }
        if (CheckforErrors())
        {
            if (Tokenstring)
            {
                delete[] Tokenstring;
                Tokenstring=nullptr;
            }
            Token.Type=Lexer::TokenType::error;
            PushTokenStack(Token);
        }
        else
        {
            Token.Type=Lexer::TokenType::endofline;
            Memcopy(Token.Data.u.cString,Tokenstring);
            PushTokenStack(Token);
        }
        LeftParenCount=0;
        RightParenCount=0;
        /* No other deletion of  memory allocated for lexer tokens is required as it is done by parser.*/
    }
    else if (strcmp(Tokenstring,"(") ==0)
    {
        LeftParenCount++;

        Token.Type=Lexer::TokenType::separator_leftParen;
        Memcopy(Token.Data.u.cString,Tokenstring);
        PushTokenStack(Token);

    }
    else if (strcmp(Tokenstring,")") ==0)
    {
        RightParenCount++;

        Token.Type=Lexer::TokenType::separator_rightParen;
        Memcopy(Token.Data.u.cString,Tokenstring);
        PushTokenStack(Token);

    }

    else if (Lexer::isCommand(Tokenstring))
    {
    	DataStructures::tolower(Tokenstring);
    	Token.Type=Lexer::TokenType::command;
        Memcopy(Token.Data.u.cString,Tokenstring);
	}
    else if (Lexer::isValidOp(Lexer::DetectOpType(Tokenstring)))
    {

        Token.Type=Lexer::TokenType::op;
        // Check other types of = operator  as += -= /= *= later
        Memcopy(Token.Data.u.cString,Tokenstring);

        if (strcmp(Tokenstring,"=") == 0)
        {
            // Check if previous token is ident,insert it into symbol table.
            //Use stack or a list to store previous, current and next token.
            // Add ident token into symbol table.
            Lexer::Token_tag PreviousToken;
            if (PopTokenStack(PreviousToken))
            {
                PushTokenStack(PreviousToken);
                // This would look up symbols as a=b=c= 1+2
                // insert symbol name (token) into symbol table.
                // code edit here
				/* 
				   Check this condition if it to be put during reading the token stack because the lexer should 
				   just add tokens to token list
				*/
                if (PreviousToken.Type== Lexer::TokenType::identifier)
                {
                    if (!CurrentScope->LookupSymbolName(PreviousToken.name))
                    {
                        CurrentScope->InsertSymbolName(PreviousToken.name);
                    }
                }
            }
            else
            {
                ErrorTracer(INCORRECT_ARITHMETIC,0,0);
            }
        }
        PushTokenStack(Token);
    }

    else if (Lexer::isIdent(Tokenstring))
    {
        Token.Type=Lexer::TokenType::identifier;
        Memcopy(Token.name,Tokenstring);
        PushTokenStack(Token);
    }
    else if (DataStructures::isNumeric(Tokenstring))
    {
        Token.Type=Lexer::TokenType::numericliteral;
        Token.Data.u.doubleValue=DataStructures::atodouble(Tokenstring);
        PushTokenStack(Token);

    }
    else if (Tokenstring==nullptr)
    {
        Token.Type=Lexer::TokenType::error;
        PushTokenStack(Token);
    }
    // add accurate error detection for syntax errors for operators as ** // */ /* +/-/ -*
    
    return Token;
}

/*
 Component Function: BOOL Lexer::isValidOp(std::size_t OpType)
 Arguments:  Op type
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
BOOL Lexer::isValidOp(std::size_t OpType)
{
    if (OpType == NOTOP)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
 Component Function: std::size_t Lexer::DetectOpType(SCHAR8 const* OpString)
 Arguments:  char string
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
std::size_t Lexer::DetectOpType(SCHAR8 const* OpString)
{
    std::size_t ReturnOpType = NOTOP;
    if ((strcmp(OpString,"+")==0))
    {
        ReturnOpType = OPPLUS;
    }
    else if ((strcmp(OpString,"-")==0))
    {
        ReturnOpType = OPMINUS;
    }
    else if ((strcmp(OpString,"/")==0))
    {
        ReturnOpType = OPDIV;
    }
    else if ((strcmp(OpString,"*")==0))
    {
        ReturnOpType = OPMUL;
    }
    else if ((strcmp(OpString,"%")==0))
    {
        ReturnOpType = OPMODULUS;
    }
    else if ((strcmp(OpString,"^")==0))
    {
        ReturnOpType = OPPOWEXPONENT;
    }
    else if ((strcmp(OpString,"=")==0))
    {
        ReturnOpType = OPEQUALTO;
    }
    else if ((strcmp(OpString,"+=")==0))
    {
        ReturnOpType = OPADDEQUALTO;
    }
    else if ((strcmp(OpString,"-=")==0))
    {
        ReturnOpType = OPMINUSEQUALTO;
    }
    else if ((strcmp(OpString,"*=")==0))
    {
        ReturnOpType = OPMULTIPLYEQUALTO;
    }
    else if ((strcmp(OpString,"/=")==0))
    {
        ReturnOpType = OPDIVEQUALTO;
    }
    else if ((strcmp((OpString),"sin")==0))
    {
        ReturnOpType = OPSIN;
    }
    else if ((strcmp((OpString),"cos")==0))
    {
        ReturnOpType = OPCOS;
    }
    else if ((strcmp((OpString),"tan")==0))
    {
        ReturnOpType = OPTAN;
    }
    else if ((strcmp(OpString,"sec")==0))
    {
        ReturnOpType = OPSEC;
    }
    else if ((strcmp(OpString,"cosec")==0))
    {
        ReturnOpType = OPCOSEC;
    }
    else if ((strcmp(OpString,"cot")==0))
    {
        ReturnOpType = OPCOT;
    }
    else if ((strcmp(OpString,"sqrt")==0))
    {
        ReturnOpType = OPSQRT;
    }
    else if ((strcmp(OpString,"loge")==0))
    {
        ReturnOpType = OPLOG;
    }
    else if ((strcmp(OpString,"<")==0))
    {
        ReturnOpType = OPLESSTHAN;
    }
    else if ((strcmp(OpString,">")==0))
    {
        ReturnOpType = OPGREATERTHAN;
    }
    else if ((strcmp(OpString,"<<")==0))
    {
        ReturnOpType = OPLEFTBITSHIFT;
    }
    else if ((strcmp(OpString,">>")==0))
    {
        ReturnOpType = OPRIGHTBITSHIFT;
    }
    else if ((strcmp(OpString,"!")==0))
    {
        ReturnOpType = OPLOGICALNOT;
    }
    else if ((strcmp(OpString,"|")==0))
    {
        ReturnOpType = OPBITWISEOR;
    }
    else if ((strcmp(OpString,"&")==0))
    {
        ReturnOpType = OPBITWISEAND;
    }
    else if ((strcmp(OpString,"&&")==0))
    {
        ReturnOpType = OPLOGICALAND;
    }
    else if ((strcmp(OpString,"||")==0))
    {
        ReturnOpType = OPLOGICALOR;
    }
    else if ((strcmp(OpString,"<=")==0))
    {
        ReturnOpType = OPLESSTHANEQUALTO;
    }
    else if ((strcmp(OpString,">=")==0))
    {
        ReturnOpType = OPGREATERTHANEQUALTO;
    }
    else if ((strcmp(OpString,"==")==0))
    {
        ReturnOpType = OPLOGICALISEQUALTO;
    }
    else if ((strcmp(OpString,"!=")==0))
    {
        ReturnOpType = OPLOGICALISNOTEQUALTO;
    }
    else if ((strcmp(OpString,"xor")==0))
    {
        ReturnOpType = OPBITWISEXOR;
    }
    else if ((strcmp(OpString,"~")==0))
    {
        ReturnOpType = OPCOMPLEMENT;
    }

    else if ((strcmp(OpString,"%=")==0))
    {
        ReturnOpType = OPMODEQUALTO;
    }
    else if ((strcmp(OpString,"~=")==0))
    {
        ReturnOpType = OPCOMPLEMENTEQUALTO;
    }
    else if ((strcmp(OpString,"_")==0))
    {
        ReturnOpType  = OPUNARYMINUS;
    }
    else if ((strcmp(OpString,"<<=")==0))
    {
        ReturnOpType  = OPLEFTSHIFTEQUALTO;
    }
    else if ((strcmp(OpString,">>=")==0))
    {
        ReturnOpType  = OPRIGHTSHIFTEQUALTO;
    }
    else if ((strcmp(OpString,"&=")==0))
    {
        ReturnOpType  = OPANDEQUALTO;
    }
    else if ((strcmp(OpString,"|=")==0))
    {
        ReturnOpType  = OPOREQUALTO;
    }
    //std::cout << "At endof DetectOpType() function " << std::endl;
    return ReturnOpType;
}

/*
 Component Function: std::size_t Lexer::DetectOpToken(Lexer::Token_tag const& OpToken)
 Arguments:  Token
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
std::size_t Lexer::DetectOpToken(Lexer::Token_tag const& OpToken)
{
    std::size_t OpType = NOTOP;
    if (OpToken.Data.u.cString)
    {

        if ((strcmp(OpToken.Data.u.cString, "+") == 0))
        {
            OpType = OPPLUS;
        }
        else if ((strcmp(OpToken.Data.u.cString, "-") == 0))
        {
            OpType = OPMINUS;
        }
        else if ((strcmp(OpToken.Data.u.cString, "/") == 0))
        {
            OpType = OPDIV;
        }
        else if ((strcmp(OpToken.Data.u.cString, "*") == 0))
        {
            OpType = OPMUL;
        }
        else if ((strcmp(OpToken.Data.u.cString, "%") == 0))
        {
            OpType = OPMODULUS;
        }
        else if ((strcmp(OpToken.Data.u.cString, "^") == 0))
        {
            OpType = OPPOWEXPONENT;
        }
        else if ((strcmp(OpToken.Data.u.cString, "=") == 0))
        {
            OpType = OPEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "+=") == 0))
        {
            OpType = OPADDEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "-=") == 0))
        {
            OpType = OPMINUSEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "*=") == 0))
        {
            OpType = OPMULTIPLYEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "/=") == 0))
        {
            OpType = OPDIVEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "_+_") == 0))
        {
            OpType = OPUNARYPLUS;
        }
        else if ((strcmp((OpToken.Data.u.cString), "sin") == 0))
        {
            OpType = OPSIN;
        }
        else if ((strcmp(OpToken.Data.u.cString, "cos") == 0))
        {
            OpType = OPCOS;
        }
        else if ((strcmp(OpToken.Data.u.cString, "tan") == 0))
        {
            OpType = OPTAN;
        }
        else if (strcmp(OpToken.Data.u.cString, "sec") == 0)
        {
            OpType = OPSEC;
        }
        else if ((strcmp(OpToken.Data.u.cString, "cosec") == 0))
        {
            OpType = OPCOSEC;
        }
        else if ((strcmp(OpToken.Data.u.cString, "cot") == 0))
        {
            OpType = OPCOT;
        }
        else if ((strcmp(OpToken.Data.u.cString, "sqrt") == 0))
        {
            OpType = OPSQRT;
        }
        else if ((strcmp(OpToken.Data.u.cString, "loge") == 0))
        {
            OpType = OPLOG;
        }
        else if ((strcmp(OpToken.Data.u.cString, "<") == 0))
        {
            OpType = OPLESSTHAN;
        }
        else if ((strcmp(OpToken.Data.u.cString, ">") == 0))
        {
            OpType = OPGREATERTHAN;
        }
        else if ((strcmp(OpToken.Data.u.cString, "<<") == 0))
        {
            OpType = OPLEFTBITSHIFT;
        }
        else if ((strcmp(OpToken.Data.u.cString, ">>") == 0))
        {
            OpType = OPRIGHTBITSHIFT;
        }
        else if ((strcmp(OpToken.Data.u.cString, "!") == 0))
        {
            OpType = OPLOGICALNOT;
        }
        else if ((strcmp(OpToken.Data.u.cString, "|") == 0))
        {
            OpType = OPBITWISEOR;
        }
        else if ((strcmp(OpToken.Data.u.cString, "&") == 0))
        {
            OpType = OPBITWISEAND;
        }
        else if ((strcmp(OpToken.Data.u.cString, "&&") == 0))
        {
            OpType = OPLOGICALAND;
        }
        else if ((strcmp(OpToken.Data.u.cString, "||") == 0))
        {
            OpType = OPLOGICALOR;
        }
        else if ((strcmp(OpToken.Data.u.cString, "<=") == 0))
        {
            OpType = OPLESSTHANEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, ">=") == 0))
        {
            OpType = OPGREATERTHANEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "==") == 0))
        {
            OpType = OPLOGICALISEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "!=") == 0))
        {
            OpType = OPLOGICALISNOTEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "xor") == 0))
        {
            OpType = OPBITWISEXOR;
        }
        else if ((strcmp(OpToken.Data.u.cString, "~") == 0))
        {
            OpType = OPCOMPLEMENT;
        }
        else if ((strcmp(OpToken.Data.u.cString, "%=") == 0))
        {
            OpType = OPMODEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "~=") == 0))
        {
            OpType = OPCOMPLEMENTEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "_") == 0))
        {
            OpType = OPUNARYMINUS;
        }
        else if ((strcmp(OpToken.Data.u.cString, "<<=") == 0))
        {
            OpType = OPLEFTSHIFTEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, ">>=") == 0))
        {
            OpType = OPRIGHTSHIFTEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "&=") == 0))
        {
            OpType = OPANDEQUALTO;
        }
        else if ((strcmp(OpToken.Data.u.cString, "|=") == 0))
        {
            OpType = OPOREQUALTO;
        }
    }

    //std::cout << "At endof DetectOpType() function " << std::endl;
    return OpType;

}

/*
 Component Function: SCHAR8 const* Lexer::DetectOpstring(const Lexer::Token_tag & OpToken)
 Arguments:  Token
 returns: Op string
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
SCHAR8 const* Lexer::DetectOpstring( const Lexer::Token_tag & OpToken)
{
    if (OpToken.Data.u.cString)
    {
        if ((strcmp(OpToken.Data.u.cString, "+") == 0))
        {
            return "+";
        }
        else if ((strcmp(OpToken.Data.u.cString, "-") == 0))
        {
            return "-";
        }
        else if ((strcmp(OpToken.Data.u.cString, "/") == 0))
        {
            return "/";
        }
        else if ((strcmp(OpToken.Data.u.cString, "*") == 0))
        {
            return "*";
        }
        else if ((strcmp(OpToken.Data.u.cString, "%") == 0))
        {
            return "%";
        }
        else if ((strcmp(OpToken.Data.u.cString, "^") == 0))
        {
            return "^";
        }
        else if ((strcmp(OpToken.Data.u.cString, "=") == 0))
        {
            return "=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "+=") == 0))
        {
            return "+=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "-=") == 0))
        {
            return "-=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "*=") == 0))
        {
            return "*=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "/=") == 0))
        {
            return "/=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "_+_") == 0))
        {
            return "_+_";
        }
        else if ((strcmp(OpToken.Data.u.cString, "sin") == 0))
        {
            return "sin";
        }
        else if ((strcmp(OpToken.Data.u.cString, "cos") == 0))
        {
            return "cos";
        }

        else if ((strcmp(OpToken.Data.u.cString, "tan") == 0))
        {
            return "tan";
        }
        else if ((strcmp(OpToken.Data.u.cString, "sec") == 0))
        {
            return "sec";
        }
        else if ((strcmp(OpToken.Data.u.cString, "cosec") == 0))
        {
            return "cosec";
        }
        else if ((strcmp(OpToken.Data.u.cString, "cot") == 0))
        {
            return "cot";
        }
        else if ((strcmp(OpToken.Data.u.cString, "sqrt") == 0))
        {
            return "sqrt";
        }
        else if ((strcmp(OpToken.Data.u.cString, "loge") == 0))
        {
            return "loge";
        }
        else if ((strcmp(OpToken.Data.u.cString, "<") == 0))
        {
            return "<";
        }
        else if ((strcmp(OpToken.Data.u.cString, ">") == 0))
        {
            return ">";
        }
        else if ((strcmp(OpToken.Data.u.cString, "<<") == 0))
        {
            return "<<";
        }
        else if ((strcmp(OpToken.Data.u.cString, ">>") == 0))
        {
            return ">>";
        }
        else if ((strcmp(OpToken.Data.u.cString, "!") == 0))
        {
            return "!";
        }
        else if ((strcmp(OpToken.Data.u.cString, "|") == 0))
        {
            return "|";
        }
        else if ((strcmp(OpToken.Data.u.cString, "&") == 0))
        {
            return "&";
        }
        else if ((strcmp(OpToken.Data.u.cString, "&&") == 0))
        {
            return "&&";
        }
        else if ((strcmp(OpToken.Data.u.cString, "||") == 0))
        {
            return "||";
        }
        else if ((strcmp(OpToken.Data.u.cString, "<=") == 0))
        {
            return "<=";
        }
        else if ((strcmp(OpToken.Data.u.cString, ">=") == 0))
        {
            return ">=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "==") == 0))
        {
            return "==";
        }
        else if ((strcmp(OpToken.Data.u.cString, "!=") == 0))
        {
            return "!=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "xor") == 0))
        {
            return "xor";
        }
        else if ((strcmp(OpToken.Data.u.cString, "~") == 0))
        {
            return "~";
        }

        else if ((strcmp(OpToken.Data.u.cString, "%=") == 0))
        {
            return "%=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "~=") == 0))
        {
            return "~=";
        }

        else if ((strcmp(OpToken.Data.u.cString, "_") == 0))
        {
            return "_";
        }
        else if ((strcmp(OpToken.Data.u.cString, "<<=") == 0))
        {
            return "<<=";
        }
        else if ((strcmp(OpToken.Data.u.cString, ">>=") == 0))
        {
            return ">>=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "&=") == 0))
        {
            return "&=";
        }
        else if ((strcmp(OpToken.Data.u.cString, "|=") == 0))
        {
            return "|=";
        }
        else
        {
            return " ";
        }
    }
    else
    {
        return " ";
    }
}


/*
 Component Function: void Lexer::PrintToken(const Lexer::Token_tag& Token)
 Arguments:
 Returns:
 Description:
  Prints a token.
  Version : 0.1
 */
void Lexer::PrintToken(const Lexer::Token_tag& Token)
{
    switch(Token.Type)
    {
    case Lexer::TokenType::identifier:
        if (Token.name && Lexer::isIdent(Token.name))
        {
            std::cout << "Token is identifier: " << Token.name << std::endl;
        }
        if (Token.Data.isDataInitialized)
        {
            std::cout << "Ident value: "<< Token.Data.u.doubleValue <<  std::endl;
        }
        else
        {
            std::cout << "(Symbol uninitialized)"<< std::endl;
        }
        break;
    case Lexer::TokenType::separator_leftParen:
        std::cout << "Token is left paren: " << Token.Data.u.cString << std::endl;
        break;
    case Lexer::TokenType::separator_rightParen:
        std::cout << "Token is right paren: " << Token.Data.u.cString << std::endl;
        break;
    case Lexer::TokenType::op:
        std::cout << "Token operator: " << Token.Data.u.cString << std::endl;
        break;
    case Lexer::TokenType::numericliteral:
        std::cout << "Token numeric literal : " << Token.Data.u.doubleValue << std::endl;
        break;
    case Lexer::TokenType::command:
        std::cout << "Entered command: " << Token.Data.u.cString << std::endl;
        break;
    case Lexer::TokenType::endofline:
        std::cout << "Token newline. " << std::endl;
        break;
    case Lexer::TokenType::error:
        std::cout << "Token error: " << Token.Data.u.cString << std::endl;
        break;
    case Lexer::TokenType::Emptytoken:
        std::cout << "Token empty " <<std::endl;
        break;
    default:
        break;
    }
}


/*
 Component Function: void Lexer::ClearToken(Lexer::Token_tag& Token)
 Arguments:
 Returns:
 Description:
  Clears token.
Version : 0.1
 */
void Lexer::ClearToken(Lexer::Token_tag& Token)
{
	Token.Type=TokenType::Emptytoken;
	Token.Data.isDataInitialized=false;

    if (Token.Data.u.cString != nullptr)
    {
        delete[] Token.Data.u.cString;
        Token.Data.u.cString=nullptr;
    }
    if (Token.name !=nullptr)
    {
        delete[] Token.name;
        Token.name=nullptr;
    }

}



/*
 Component function: SCHAR8* Lexer::getTokencharstring(void)
 Arguments: char string
 returns : a substring of type SCHAR8*
 Version : 0.1
 Description:
 parses a char string into substrings.
 Each substring canbe these:
 ident,
 numeric,
 operator
 parenthesis ( )
 These substrings would be read by Tokenprocessor() function.

  Notes:
 

 Delete substring only in this module.
 
Try to make sure count+1 < length in all occasions.

 */
SCHAR8* Lexer::getTokencharstring(void)
{
    /* Gather all strings separated by ' ' , operator and ( ) . Detect if that string is a ident or numeric rather than parsing each character.
    */
    static SCHAR8* str=nullptr;
    static SCHAR8* substring=nullptr;
    BOOL IdentDetected=false;
    static std::size_t length=0;

    //std::cout << "Length of char string " << length << std::endl;
    SCHAR8 ch= ' ';
    //create substring and send it to token processor which sends it to getNextToken() each time getNextToken() is called.

    std::size_t Count=0;
    /*  separate counter for each type of substring */
    SCHAR8 NextChar =' ';
    static std::size_t i=0;
    BOOL SubstringdetectionComplete=false;
    if (i == length || i==0)
    {
        i=0;
        //std::cout << "deleting input string" << std::endl;
        str=io::getInput();
        length=DataStructures::strlength(str);

        //std::cout << "reached end of string while parsing"  << std::endl;
    }



    if (substring !=nullptr)
    {
        //std::cout << "Substring is not empty, deleting it  "  << std::endl;
        delete[] substring;
        substring=nullptr;

    }
    // Is it necessary to allocate size of string to be a full length of line entered.or just allocate where a substring was extracted.
    substring = new SCHAR8[length+2]; /* +2 for newline and line terminator for single char strings*/
    

    //std::cout << "allocating memory for newline and Substring."  << std::endl;
    while(i<length && SubstringdetectionComplete== false && Count+1 < length)
    {
        ch = str[i];
        if (Lexer::isWhitespace(ch))
        {
            Count=0;
            Lexer::SkipWhitespace(str,length,i);
        }
        else  if ( ch == '\n')
        {
            Count=0;
            //std::cout << "character is new line" << std::endl;
            /* Check if \n positoin is at length of char string.*/
            substring[Count]= ch;
            substring[Count+1]='\0';
            SubstringdetectionComplete=true;
        }
        /*
        There is some ambiguity in digits of a numeric string and a identifier

        */
        else if ((DataStructures::isDigit(ch) )|| ch == '.'  || (Lexer::isIdentChar(ch)))
        {

            if (Count==0)
            {
                if (ch == '_' || (DataStructures::isAlphabet(ch) ))
                {
                    IdentDetected=true;
                }

            }
            if (!IdentDetected)
            {

                /* Detect strings as 1a+ 3b . 1a should produce a error instead of tokens 1 and a */
                substring[Count]=ch;
                //std::cout << " before PeekNextchar()"  << std::endl;
                NextChar=Lexer::PeekNextChar(str,length,i);
                if (((!Lexer::isIdentChar(NextChar)) &&  NextChar != '.') || NextChar == '#' )
                {
                    //std::cout <<  "substring is ident " << std::endl;
                    // Create end of substring here */
                    substring[Count+1]= '\0';
                    SubstringdetectionComplete=true;
                    IdentDetected=false;
                    //std::cout << "Substring detected is " << substring << std::endl;
                }
                else
                {
                    Count++;
                }
            }
            else
            {
                substring[Count]=ch;
                //std::cout << " before PeekNextchar()"  << std::endl;
                NextChar=Lexer::PeekNextChar(str,length,i);

                if ((!Lexer::isIdentChar(NextChar)) || NextChar == '#' )
                {
                    //std::cout <<  "substring is ident " << std::endl;
                    // Create end of substring here */
                    substring[Count+1]= '\0';
                    SubstringdetectionComplete=true;
                    IdentDetected=false; // change this flag to ident detection complete. 
                    //std::cout << "substring is " << substring << std::endl;
                }
                else
                {
                    Count++;
                }
            }

        }

        /* Write code for these two tokens */
        else  if (Lexer::isSeparator(ch))
        {
            /* Detect nested ()  as (((1*4)*2)*3) */
            Count=0;
            substring[Count]=ch;
            substring[Count+1]='\0';
            SubstringdetectionComplete=true;

        }
        else if (Lexer::isOperchar(ch))
        {
            substring[Count]=ch;
            /* Detect operators as ++, -- , +=,. *= , /=., a= !b, a=-b or a=+b.
            Detect a/=b, a*=b, a+=b, a%=b, a-=b. */
            NextChar=Lexer::PeekNextChar(str,length,i);
            if ((ch == '=') && (NextChar == '~' || NextChar == '!'|| NextChar == '-' || NextChar == '+'))
            {
                substring[Count+1]='\0';
                SubstringdetectionComplete=true;
            }
            /*
            Create a map of operators which match. This algoritm should produce a syntax error
            for opertors which dont match when placed consequtively .
            Move this error detection into construction of expression tree.*/
            else if ((ch == '=') && (NextChar == '*' || NextChar == '/'|| NextChar == '^' || NextChar == '%'))
            {

                substring[Count+1]=NextChar;
                substring[Count+2]='\0';
                i++;
                SubstringdetectionComplete=true;
                ErrorTracer(SYNTAX_ERROR,0,0);

                /* detect error in parser */
            }
            else if ((ch == '*') && (NextChar == '*' || NextChar == '/'|| NextChar == '^' || NextChar == '%'))
            {

                substring[Count+1]=NextChar;
                substring[Count+2]='\0';
                i++;
                SubstringdetectionComplete=true;
                ErrorTracer(SYNTAX_ERROR,0,0);
                /* detect error in parser */
            }
            else if ((ch == '/') && (NextChar == '*' || NextChar == '/'|| NextChar == '^' || NextChar == '%'))
            {

                substring[Count+1]=NextChar;
                substring[Count+2]='\0';
                i++;
                SubstringdetectionComplete=true;
                ErrorTracer(SYNTAX_ERROR,0,0);
                /* detect error in parser */
            }
            else if ((ch == '+') && (NextChar == '*' || NextChar == '/'|| NextChar == '^' || NextChar == '%'))
            {

                substring[Count+1]=NextChar;
                substring[Count+2]='\0';
                i++;
                SubstringdetectionComplete=true;
                ErrorTracer(SYNTAX_ERROR,0,0);
                /* detect error in parser */
            }
            else if ((ch == '-') && (NextChar == '*' || NextChar == '/'|| NextChar == '^' || NextChar == '%'))
            {

                substring[Count+1]=NextChar;
                substring[Count+2]='\0';
                i++;
                SubstringdetectionComplete=true;
                ErrorTracer(SYNTAX_ERROR,0,0);
                /* detect error in parser */
            }
            else if (!Lexer::isOperchar(NextChar))
            {
                // Create end of substring here */
                substring[Count+1]= '\0';
                Count=0;
                SubstringdetectionComplete=true;
            }
            else
            {
                Count++;
            }
        }

        else
        {
            substring[Count]= ch;
            substring[Count+1]= '\0';
            SubstringdetectionComplete=true;
            ErrorTracer(ILLEGAL_CHARACTER,0,0);
        }

        i++;
    }
    //std::cout << "Substring at return statement " << substring << std::endl;
    //std::cout << "substring in getTokencharString: " << substring << std::endl;
    return substring; // This is appending \0 at the end of each string.
}

