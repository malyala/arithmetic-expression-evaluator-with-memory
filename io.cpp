/*
Name : Io.cpp
Author: Amit Malyala, Copyright Amit Malyala 2016
Description:
Read input into a string.
Version: 0.1
*/
#include "Io.h"
#include <cstring>

/* Get input and return a char string*/
/*
Notes:
*/
SCHAR8* io::getInput(void)
{
    SCHAR8 ch = 0;
    bool EndofInput = false;
    std::size_t Strsize=100, index=0;
    static SCHAR8* charString =nullptr ;
    //std::unique_ptr<SCHAR8*> chartring = std::make_unique<SCHAR8*>(Strsize);
    if (charString!=nullptr)
    {
        delete[] charString;
        charString=nullptr;
    }
    charString = new SCHAR8[Strsize];
    if (charString)
    {
        /*
        Read input until ENTER is pressed (until '\n' is detected).
        */
        while(EndofInput== false )
        {
            if (index +2 ==Strsize)
            {
                // Resize charString
                SCHAR8* AnotherString = new SCHAR8[Strsize+10];
                if (AnotherString)
                {
                    for (std::size_t i=0; i<Strsize; i++)
                    {
                        AnotherString[i]=charString[i];
                    }
                    Strsize+=10;
                    delete[] charString;
                    charString= AnotherString;
                }
                else
                {
                    ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
                    EndofInput=true;
                    charString=nullptr;
                }
            }

            if (!EndofInput)
            {
                ch = static_cast <SCHAR8> (std::cin.get() );
                if(ch == '\n' )
                {
                    //std::cout << "Entered new line " << std::endl;
                    // append new line char correctly
                    charString[index]='\n';
                    charString[index+1]='\0';
                    // End of input marker is '\n'
                    EndofInput =true;
                }
                else
                {
                    charString[index]= ch;
                    index++;
                }
            }
        }
    }
    else
    {
        charString=nullptr;
        ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
    }
    //std::cout << "char string in getInput() " << charString << std::endl;
    return charString;
}
