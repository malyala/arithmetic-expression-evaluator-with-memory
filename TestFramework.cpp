#include "Lexer.h"

#include "Lexer.h"
#include "Stack.h"
#include <cstring>
// Comment when module is integrated with the project
// Uncomment to develop or test

/* Declare external scope */
extern Scopes* CurrentScope;


/* Test case 1 - char strings init */
void TestCharStrings(void);


#if(0)
SINT32 main(void)
{

	return 0;
}
#endif


/* Test case 1 - char strings init */
void TestCharStrings(void)
{
	Lexer::Token_tag Token;
	Lexer::Token_tag NewToken;
	Token.Data.u.cString = new char[6];
	Token.Data.u.cString[0]='h';
	Token.Data.u.cString[1]= 'e';
	Token.Data.u.cString[2]='l';
	Token.Data.u.cString[3]='l';
	Token.Data.u.cString[4]='o';
	Token.Data.u.cString[5]='\0';
	
	
	Token.name = new char[6];
	Token.name[0]='h';
	Token.name[1]= 'e';
	Token.name[2]='l';
	Token.name[3]='l';
	Token.name[4]='o';
	Token.name[5]='\0';
	
	
	NewToken=Token;
	
	std::cout << NewToken.Data.u.cString << '\n'; 
	std::cout << Token.Data.u.cString << '\n'; 
	
	std::cout << NewToken.name << '\n'; 
	std::cout << Token.name << '\n'; 
	// assignment works after adding \0 char
}
