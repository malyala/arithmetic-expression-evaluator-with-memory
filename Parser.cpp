/*
Name :  Parser.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module initliazes parser
Notes:

Bug and Version history:
0.1 Initial version
Notes: construct syntax tree or binary expression tree.
The parser would get each token and construct a tree. It would either add each token into a tree or evaluate that tree which is being
constructed.This is based on token newline and if any '=' operator was present in the expression.

If '=' is detected in expression, then symbol is updated in symbol table or else the expression is evaluated after Newline token.

Operator table in decreasing order of precedence
-------------------------------------------------
(sin,cos,tan,sec,cosec,cot,loge, pow) -O1
								   ~  -O2
								   !  -O3
						  unary minus -O4
						   unary plus -O5
							  (* / %) -O6
								 (+-) -O7
							( << >> ) -O8
						  (< <= > >= )-O9
							( ==, != )-O10
								  &   -O11
								 xor  -O12
								  |   -O13
								 &&   -O14
								 ||   -O15

								  =	  -O16      Direct assignment (provided by default for C++ classes)
							+=   -=	  -O17      Compound assignment by sum and difference
						  *=   /=   %= -O18	    Compound assignment by product, quotient, and remainder
							 <<=   >>= -O19	    Compound assignment by bitwise left shift and right shift
						   &=   ^=  |= -O20

To do:

Add operators:
								  =	  -O16      Direct assignment (provided by default for C++ classes)
							+=   -=	  -O17      Compound assignment by sum and difference
						  *=   /=   %= -O18	    Compound assignment by product, quotient, and remainder
							 <<=   >>= -O19	    Compound assignment by bitwise left shift and right shift
						   &=   ^=  |= -O20



Notes:

Change parseTokenstream function to detect
+1+1
This should be equivalent oto 1+1

Known issues:

To do:


Coding log:
18-06-19  Code adapted from a previous baseline. Project compiles in G++ under -std=c++14
19-06-20  Added components Lexer::isCommand() in Lexer::Tokenprocessor()  , Datastructures::isAlpha() Datastructures::tolower() functions. 
          Moved strlength() function into namespace Datastructures.
          

*/
#include "Parser.h"

// Tree pointer for Root
Tnode *Root = new Tnode;

/* Is equalto Op detected */
BOOL equaltoopdetected =false;

/*Create a symbol table for all idents. All symbols would be in one scope.*/
std::unique_ptr <Scopes> CurrentScope = std::make_unique<Scopes>();
#if(0)
/* Comment to test module. Uncomment to integrate */
SINT32 main(void)
{
START:
    try
    {
        Parser::ParseandEvaluate();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        goto START;
    }
    return 0;
}
#endif

/*
  Component Function: void Parser::ParseandEvaluate(void)
  Arguments: None
  Returns: None
  Description:
  Initializes the parser
  Version : 0.1
  Known issues:
  Write funtionality for unary plus and nary minus.
*/
void Parser::ParseandEvaluate(void)
{
    BOOL IdentsCleared=false;
    InitStack();
    InitErrorTracer();
    Lexer::Token_tag Token;
    Lexer::Token_tag TempToken;
    while (1)
    {
        Token = Lexer::getNextToken();
        if  (Token.Type == Lexer::TokenType::error)
        {
            while (!isTokenStackEmpty())
            {
                /* Delete memory allocated for char strings */

                PopTokenStack(TempToken);

                if (TempToken.Data.u.cString !=nullptr)
                {
                    delete[] TempToken.Data.u.cString;
                }
            }
            while (!isPostfixStackEmpty())
            {
                /* Delete memory allocated for char strings */

                PopTokenPostfixStack(Token);
                if (Token.Data.u.cString)
                {
                    delete[] Token.Data.u.cString;
                }
                if (Token.name)
                {
                    delete[] Token.name;
                }
            }

            InitStack();
            InitErrorTracer();
            PrintErrors();

        }
        else if (Token.Type == Lexer::TokenType::op)
        {
            std::size_t OpType = Lexer::DetectOpType(Token.Data.u.cString);
            if (OpType == OPEQUALTO || OpType == OPADDEQUALTO || OpType == OPMINUSEQUALTO || OpType == OPMODEQUALTO \
                    || OpType == OPDIVEQUALTO || OpType == OPCOMPLEMENTEQUALTO || OpType == OPMULTIPLYEQUALTO)
            {
                equaltoopdetected = true;
            }
        }
        else if (Token.Type == Lexer::TokenType::command)
        {
            // The exit function should delete all memory allocated
            if (strcmp("exit", (Token.Data.u.cString)) == 0)
            {
                Lexer::PrintToken(Token);
                CurrentScope->Delete(CurrentScope->GetCurrentLevel());
                InitStack();
                InitErrorTracer();
                break;
            }
            else if (strcmp("clear", (Token.Data.u.cString)) == 0)
            {
                Lexer::PrintToken(Token);
                if (Token.Data.u.cString)
                {
                    delete[] Token.Data.u.cString;
                    Token.Data.u.cString = nullptr;
                }
                CurrentScope->EraseAllData();
                IdentsCleared=true;
            }
        }

        else if (Token.Type == Lexer::TokenType::endofline)
        {
            ReverseTokenStack();
            while ((!isTokenStackEmpty()) && (!IdentsCleared))
            {
                /* Delete memory allocated for char strings */
                /* detect = op and construct expression tree.Print result only if = operator is not detected.*/
                /* Detect a=b=1 type expressions */
                /* Construct expression tree consisely create subtrees and keep on adding to the main tree
                or instead of constructing the tree from RPN stack? */
                /*
                Construct tree only once for exisiting idents on lhs in a expression with = operator.
                It is being done many times with each \n' token erasing value stored for that ident in the tree.
                If the program recognizes a new ident, construct the tree again or else modify the tree. Perhaps the tree
                should be constructed with each token rather than reading all tokens from RPN stack. In this way it is possible
                to find out if new tree can be created or that node can be modified.

                a= 1
                a= 3
                Here, node for symbol "a" should not be created twice.
                */

                Lexer::ClearToken(Token);
                PopTokenStack(Token);
                // A new token is intialized with value of a previous ident initialized with = operator !!!!!!!!!*/
                //std::cout << "Printing tokens inside Tokenstack " << std::endl;
                //Lexer::PrintToken(Token);
                /* Construct Postfix stack with Token Stack */
                Parser::ParseTokenstream(Token);
            }
            if (!IdentsCleared)
            {
            	
            	// Too many checks for error list. Try to print all errors only once somewhere at the exit of application.
                if (!CheckforErrors())
                {
                    ReversePostfixStack();
                    // Print postfix stack
                    //PrintPostfixStack();
                    /* Evaluate tree and print it */
                    Parser::BinaryTree::ConstructBinaryExpressionTree();
                }
                else
                {
                    PrintErrors();
                }
                if (CheckforErrors())
                {
                    PrintErrors();
                }
                else
                {
                    //std::cout << "Evaluating expression" << std::endl;
                    Parser::BinaryTree::EvaluateExpression();
                   
                   if (CheckforErrors())
                   {
                       PrintErrors();
                   }
                   
                }
            }
            /* store all previously declared idents with = operator in Symbol Table */
            //Parser::BinaryTree::DeleteBinaryTree(Root);
            InitStack();
            InitErrorTracer();
            IdentsCleared=false;
            equaltoopdetected=false;
        }

    }
}

/*
Use a stack to push all tokens into a stack.
construct expression tree after detecting  = operator
Assign result of expression to lvalue of = operator
if there is no  = operator, construct and evalulate expression tree.
if there are one or more idents in expression, look up their value from symbol table, consrruct a tree, evaluate it and print it.
if there are one or more operands in expresssion, construct tree, evaluate and print it.

*/


/*
Component Function: void  Parser::ParseTokenstream(Lexer::Token_tag& CurrentToken)
Arguments: Token stream
Returns: None
Version : 0.1 Initial version.
Description:
Notes:
To do:

fix unary plus and unary minus in expressions.

1/-3
13/+5


Add operators:
								  =	  -O16      Direct assignment (provided by default for C++ classes)
							+=   -=	  -O17      Compound assignment by sum and difference
						  *=   /=   %= -O18	    Compound assignment by product, quotient, and remainder
							 <<=   >>= -O19	    Compound assignment by bitwise left shift and right shift
						   &=   ^=  |= -O20


Known issues:
unary plus code. This has to be fixed.
postfix stack is wrong for
+1+2
Res:
1+2+

+1+(2)
Res:
1+2+

Change sign for unary plus.
This gives correct output for
+(1+2)
12 + _+_

To do:
Delete memory allocated for char string and name in Token structs. or use smart pointers as shared_ptr
*/
void  Parser::ParseTokenstream(Lexer::Token_tag& CurrentToken)
{
    Lexer::Token_tag Temp;
    Lexer::Token_tag OpTemp;
    BOOL IsUnaryMinus = false;
    BOOL Unaryplus = false;
    Lexer::Token_tag NextToken;
    Lexer::Token_tag PreviousToken;
    static BOOL execonce = false;
    std::size_t OpType = NOTOP;
    Lexer::ClearToken(Temp);
    Lexer::ClearToken(OpTemp);

    if (execonce == false)
    {
        Lexer::ClearToken(PreviousToken);
        PreviousToken.Type = Lexer::TokenType::Emptytoken;
        execonce = true;
    }
    /* Get Next Token */

    if (PopTokenStack(NextToken))
    {
        PushTokenStack(NextToken);
    }
    else
    {
        NextToken.Type = Lexer::TokenType::Emptytoken;
    }
     /*
    if (CurrentToken.Type != Lexer::TokenType::endofline)
    {

        std::cout << "Previous token " << std::endl;
        Lexer::PrintToken(PreviousToken);
        std::cout << "Current Token " << std::endl;
        Lexer::PrintToken(CurrentToken);
        std::cout << "Next token " << std::endl;
        Lexer::PrintToken(NextToken);
    }
     */
    /* Write shuting yard algorithm here */
    switch (CurrentToken.Type)
    {
    case Lexer::TokenType::separator_rightParen:
        if (NextToken.Type == Lexer::TokenType::separator_leftParen || NextToken.Type == Lexer::TokenType::identifier)
        {

        }
        else
        {
            if (PopTokenOpStack(Temp))
            {
                PushTokenOpStack(Temp);
                /* This line should be checked */
                while (!isOpStackEmpty())
                {
                    PopTokenOpStack(OpTemp);
                    if ((OpTemp.Type == Lexer::TokenType::separator_leftParen))
                    {
                        break;
                    }
                    else
                    {
                        PushTokenPostfixStack(OpTemp);
                    }
                }
            }
            else
            {
                /* Allow some internal errors to not have line numbers.Line and column numbers are mostly useful for syntax errors.
                ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
                */
            }
        }
        break;
    case Lexer::TokenType::Emptytoken:
        break;
    case Lexer::TokenType::error:
        break;


    case Lexer::TokenType::separator_leftParen:
        /* std::cout << "\nDetected Left parenthesis"; */
        PushTokenOpStack(CurrentToken);
        break;

    case Lexer::TokenType::numericliteral:
        CurrentToken.Data.isDataInitialized=true;
        CurrentToken.Data.Type=DOUBLETYPE;
        PushTokenPostfixStack(CurrentToken);
        break;
    case Lexer::TokenType::identifier:
        CurrentToken.Data.isDataInitialized=false;
        CurrentToken.Data.Type=IDENT;
        PushTokenPostfixStack(CurrentToken);
        break;

    case Lexer::TokenType::op:
        OpType = DetectOpToken(CurrentToken);
        if (Lexer::isValidOp(OpType))
        {
            if ((strcmp( "+", Lexer::DetectOpstring(CurrentToken)) ==0))
            {
                /*
                 std::cout << "\nDetected an operator O1 " << Token.Data.u.cString;
                */
                Lexer::ClearToken(Temp);
                /*
                 Unary plus code begin
                 Change this code. The unary minus  or plus can also be pushed into op stack for correct arithmetic on a expression as -1-1 , +1+-1
                 */
                /*
                Scnarios for unary plus :
                +1+1
                (+2+5)
                +1+2+141-3
                +(1+2)
                + cos(45)
                1+ +4
                */
                if ((((PreviousToken.Type == Lexer::TokenType::numericliteral) || (PreviousToken.Type == Lexer::TokenType::identifier))  )  &&
                        (NextToken.Type == Lexer::TokenType::op))
                {
                    Unaryplus = false;
                    /* std::cout << "\nOperator o1 is unary plus"; */
                }
                else if ((((PreviousToken.Type == Lexer::TokenType::numericliteral) || (PreviousToken.Type == Lexer::TokenType::identifier))  )  &&
                         ((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier)))
                {
                    Unaryplus = false;
                    /* std::cout << "\nOperator o1 is unary plus"; */
                }
                else if ( ((PreviousToken.Type == Lexer::TokenType::numericliteral) || (PreviousToken.Type == Lexer::TokenType::identifier) ||\
                           ( PreviousToken.Type ==Lexer::TokenType::separator_rightParen )) && NextToken.Type == Lexer::TokenType::separator_leftParen)
                {
                    Unaryplus = false;
                    /* std::cout << "\nOperator o1 is unary plus"; */
                }
                else if ((PreviousToken.Type == Lexer::TokenType::separator_rightParen) &&
                         ((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier ||\
                                 NextToken.Type == Lexer::TokenType::separator_leftParen)))
                {
                    Unaryplus = false;
                }
                else if ((PreviousToken.Type == Lexer::TokenType::Emptytoken)  &&
                         ((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier)))
                {
                    Unaryplus = true;
                    /* std::cout << "\nOperator o1 is unary plus"; */
                }
                else if ((PreviousToken.Type == Lexer::TokenType::Emptytoken)  &&
                         NextToken.Type == Lexer::TokenType::separator_leftParen)
                {
                    Unaryplus = true;
                    /* std::cout << "\nOperator o1 is unary plus"; */
                }
                else if ((PreviousToken.Type == Lexer::TokenType::separator_leftParen)  &&
                         ((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier)))
                {
                    Unaryplus = true;
                    /* std::cout << "\nOperator o1 is unary plus"; */
                } /* for (+1+2) */

                else if ( PreviousToken.Type==Lexer::TokenType::op && ((NextToken.Type == Lexer::TokenType::numericliteral) || \
                          (NextToken.Type == Lexer::TokenType::identifier)))
                {
                    Unaryplus = true;
                    /* std::cout << "\nOperator o1 is unary plus"; */
                }/* for +(1+2) */
                else if ((((PreviousToken.Type == Lexer::TokenType::numericliteral) || (PreviousToken.Type == Lexer::TokenType::identifier || PreviousToken.Type==Lexer::TokenType::op ))  )  &&
                         ( (NextToken.Type == Lexer::TokenType::endofline)))
                {
                    ErrorTracer(INCORRECT_ARITHMETIC,0,0);
                }
                if (Unaryplus)
                {
                    delete[] CurrentToken.Data.u.cString;
                    CurrentToken.Data.u.cString =nullptr;
                    SCHAR8* Sym = new SCHAR8[4];
                    Sym[0] = '_';
                    Sym[1] = '+';
                    Sym[2] = '_';
                    Sym[3] = '\0';
                    Memcopy(CurrentToken.Data.u.cString, Sym);
                    delete[] Sym;
                    Sym = nullptr;
                    OpType=OPUNARYPLUS;
                    Unaryplus = false;
                    //std::cout << "Found unary plus ---------------------------------------------" << std::endl;
                    //std::cout << "Printing unary plus "  << CurrentToken.Data.u.cString << std::endl;
                    Parser::ParseOperatorandOperand(Temp, OpTemp, CurrentToken, OpType);
                }
                else

                {
                    //std::cout << "Found normal plus ----------------------------------------------" << std::endl;
                    Parser::ParseOperatorandOperand(Temp, OpTemp, CurrentToken, OpType);
                }

            }
            else if ((strcmp( "-", Lexer::DetectOpstring(CurrentToken)) ==0))
            {

                // use string comparision directly instead of DetecOpToken() function as many operators defines have same constant values. Do the same for op +.
                // for -1+1 -1-1 -1*1 -1/1
                if(((PreviousToken.Type == Lexer::TokenType::numericliteral || PreviousToken.Type== Lexer::TokenType::identifier)) \
                        && NextToken.Type== Lexer::TokenType::op)
                {
                    // for 1- -4
                    IsUnaryMinus = false;
                }
                else if (((PreviousToken.Type == Lexer::TokenType::numericliteral || PreviousToken.Type== Lexer::TokenType::identifier)) &&
                         ((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier) ||\
                          (NextToken.Type == Lexer::TokenType::separator_leftParen)))
                {
                    IsUnaryMinus = false;
                }
                else if ((PreviousToken.Type == Lexer::TokenType::separator_rightParen) &&
                         ((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier ||\
                                 NextToken.Type == Lexer::TokenType::separator_leftParen)))
                {
                    IsUnaryMinus = false;
                }
                else if (PreviousToken.Type == Lexer::TokenType::op && ((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier)))
                {
                    IsUnaryMinus = true;
                }
                else if ((PreviousToken.Type == Lexer::TokenType::Emptytoken || PreviousToken.Type == Lexer::TokenType::endofline) && ((NextToken.Type == Lexer::TokenType::numericliteral) || \
                         (NextToken.Type == Lexer::TokenType::identifier)))
                {
                    IsUnaryMinus = true;
                    //std::cout << "\noperator o1 is unary minus";
                }// for -(1+2)
                else if (PreviousToken.Type == Lexer::TokenType::Emptytoken && (NextToken.Type == Lexer::TokenType::separator_leftParen))
                {
                    IsUnaryMinus = true;
                    //std::cout << "\noperator o1 is unary minus";
                }// for (-1+2)
                else if ((PreviousToken.Type == Lexer::TokenType::separator_leftParen) &&
                         ((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier)))
                {
                    IsUnaryMinus = true;
                    //std::cout << "\nOperator o1 is unary minus";
                }// if 1+-1 1*-1 1/-1 1--1
                // -sin(90)
                else if (((strcmp( "-", Lexer::DetectOpstring(CurrentToken)) ==0)) && ((strcmp( "sin", Lexer::DetectOpstring(NextToken)) ==0) || (strcmp( "cos", Lexer::DetectOpstring(NextToken)) || (strcmp( "tan", Lexer::DetectOpstring(NextToken))==0) || (strcmp( "sec", Lexer::DetectOpstring(NextToken)) ==0) \
                         || (strcmp( "cosec", Lexer::DetectOpstring(NextToken)) ==0) || (strcmp( "cot", Lexer::DetectOpstring(NextToken))==0) || (strcmp( "sqrt", Lexer::DetectOpstring(NextToken)) ==0) || (strcmp( "^", Lexer::DetectOpstring(NextToken))==0) \
                         || (strcmp( "loge", Lexer::DetectOpstring(NextToken))==0)  )))
                {
                    IsUnaryMinus = true;
                    //std::cout << "\noperator o1 is unary minus";
                }


                if (IsUnaryMinus)
                {
                    delete[] CurrentToken.Data.u.cString;
                    CurrentToken.Data.u.cString =nullptr;
                    SCHAR8* Sym = new SCHAR8[2];
                    Sym[0] = '_';
                    Sym[1] = '\0';
                    Memcopy(CurrentToken.Data.u.cString, Sym);
                    delete[] Sym;
                    Sym = nullptr;
                    // Process this unary minus OP like the rest.

                    OpType=OPUNARYMINUS;
                    //std::cout << "Found unary minus  " << std::endl;
                    Parser::ParseOperatorandOperand(Temp, OpTemp, CurrentToken, OpType);  // code Change here
                    IsUnaryMinus = false;
                }
                else
                {
                    //std::cout << "Found normal minus" << std::endl;
                    Parser::ParseOperatorandOperand(Temp, OpTemp, CurrentToken, OpType);
                }
            }
            //sqrt() has same precedence as any function such as sin() cos() and other functions.
            else
            {

                Parser::ParseOperatorandOperand(Temp, OpTemp, CurrentToken, OpType);
            }
        }
        break;

    case Lexer::TokenType::endofline:
        while (!isOpStackEmpty())
        {
            Lexer::ClearToken(Temp);
            PopTokenOpStack(Temp);
            if (Temp.Type != Lexer::TokenType::separator_leftParen)
            {
                PushTokenPostfixStack(Temp);
            }
        }
        execonce=false;
        break;
    default:
        break;
    }
    Lexer::ClearToken(PreviousToken);
    PreviousToken = CurrentToken;
}

/*
Component Function: Parser::ParseOperatorandOperand(Lexer::Token_tag& Temp,Lexer::Token_tag& OpTemp,Lexer::Token_tag& CurrentToken,std::size_t& CurrentOpType)
Arguments: Temp and OP temp, CurrentToken
Returns: None
Description:  Modifies RPN stack for each operator . Detect current operator and operator
Notes:
Use operator table in this function detecing current Operator?
Version: 0.1 Initial version

Notes:

 error -do not pop all operators from op stack into output stack as underlying operators may have low precedence
than incoming operator. Implement a operator comparator stack to pop operators from op stack to output stack which dont match incoming operators
in a temporary operators stack.

Compare priority of incoming op with priority of every op on op stack . If priority of op in Op stack is more, displace the op into output stack
or else not. Do this for every OP on OP stack.

	Example:

	Op stack:
	5
	4
	1

	Current token OP:
	3

	3 displaces 4 and 5, 1 in Op stack into output stack although 1 should be present in OP stack and the new OP stack should be:
	3
	1

Example:

   Op stack
   *
   =

   Current OP:
   +

   Op stack
   +
   =

   Output stack:
   *


   Construct Postfix stack with unary plus correctly.
*/
void  Parser::ParseOperatorandOperand(Lexer::Token_tag& Temp, Lexer::Token_tag& OpTemp, Lexer::Token_tag& CurrentToken, std::size_t& CurrentOp)
{
    Lexer::ClearToken(Temp);
    Lexer::ClearToken(OpTemp);
    std::size_t OPonTopofOPStack = NOTOP;
    if (PopTokenOpStack(Temp))
    {
        OPonTopofOPStack = Lexer::DetectOpToken(Temp);
        /* Lower number op means higher precedence according to Lexer.h*/
        /* Detect  if top token in output stack is ( Pop the token, discard it, push current token which is a op onto Op stack. */
        if (Temp.Type== Lexer::TokenType::separator_leftParen)
        {
            PushTokenOpStack(Temp);
            PushTokenOpStack(CurrentToken);

        }
        else if (CurrentOp > OPonTopofOPStack)
        {

            PushTokenPostfixStack(Temp);
            /* Get all ops with precedence higher than CurrentOpType and displace them into output stack */
            DetectUnderlyingOperatorsinOpStack(OpTemp,CurrentOp);
            PushTokenOpStack(CurrentToken);
        }
        else if (CurrentOp == OPonTopofOPStack)
        {
            PushTokenPostfixStack(Temp);
            PushTokenOpStack(CurrentToken);
        }
        else
        {
            PushTokenOpStack(Temp);
            PushTokenOpStack(CurrentToken);
        }
    }
    else
    {
        PushTokenOpStack(CurrentToken);
    }

}

/* Parse operators*/
void Parser::DetectUnderlyingOperatorsinOpStack(Lexer::Token_tag& OpTemp, std::size_t& CurrentOp)
{
    BOOL OperatorsProcessed = false;
    std::size_t OponTopofOpStack = NOTOP;
    /* Combine this condition with outer function . This condition should be checkd in outer function? */
    while ((!isOpStackEmpty()) && OperatorsProcessed == false)
    {
        PopTokenOpStack(OpTemp);
        OponTopofOpStack = DetectOpToken(OpTemp);
        /* Compare priority of op in opstack with incoming op here. OP priprity number 1 / OP prioprity 15 in Lexe.h*/
        if (CurrentOp > OponTopofOpStack)
        {
            PushTokenPostfixStack(OpTemp);
        }
        else if (CurrentOp == OponTopofOpStack)
        {
            PushTokenPostfixStack(OpTemp);
            OperatorsProcessed = true;
        }
        else
        {
            OperatorsProcessed = true;

            PushTokenOpStack(OpTemp);

        }
    }
}

/* Look up token in Symbol Table*/

BOOL Parser::LookupNameinSymbolTable(Lexer::Token_tag& Token)
{
    BOOL TokenFound=false;
    SymbolData TempData {false,nullptr};
    if (CurrentScope->LookupSymbolName(Token.name))
    {
        TokenFound=true;
        TempData=CurrentScope->GetSymbolData(Token.name);
        if (TempData.isFound)
        {
            if (TempData.pEntry->GetData().isDataInitialized)
            {
                Token.Data=TempData.pEntry->GetData();
                Token.Data.isDataInitialized=true;
            }
        }
    }
    return TokenFound;
}


/*
Component Function: void Parser::BinaryTree::InsertNode(std::size_t NodePos,Tnode *Tree,Tnode *NewNode)
Arguments: Node position, new node.
Returns : None.
Description:
This function inserts a node into a binary expression tree. A node is
inserted as a left or right node.
Version and bug history:
0.1 Initial version.
0.2 Added left and right operands, parent node linkage.
0.3 Removed nullptr assignment to leftchild and RightChild. Added nullpointer check for Tnode *Tree
0.4 Changed function for lexer tokens in Tree nodes.
To do:

Notes:
For now we will add nodes directly into the root tree.
 */

void Parser::BinaryTree::InsertNode(std::size_t NodePos, Tnode *Tree, Tnode *NewNode)
{
    if (Tree != nullptr)
    {
        NewNode->pParent = Tree;
        /* use lexter tokens such as
        numericliteral
        identifier
        op
        */
        if (NewNode->Token.Type == Lexer::TokenType::op  )
        {
            if (NodePos == NODE_POSITION_LEFT_SUBTREE)
            {
                Tree->LeftChild = NewNode;
            }
            else if (NodePos == NODE_POSITION_RIGHT_SUBTREE)
            {
                Tree->RightChild = NewNode;
            }

        }
        else if (NewNode->Token.Type == Lexer::TokenType::numericliteral ||NewNode->Token.Type == Lexer::TokenType::identifier)
        {
            // a numeric literal cant be on lhs of = operator.
            AssignChildNodeNull(NewNode,NODE_POSITION_LEFT_SUBTREE);
            AssignChildNodeNull(NewNode,NODE_POSITION_RIGHT_SUBTREE);
            if (NodePos == NODE_POSITION_LEFT_SUBTREE)
            {
                Tree->LeftChild = NewNode;
            }
            else if (NodePos == NODE_POSITION_RIGHT_SUBTREE)
            {
                Tree->RightChild = NewNode;
            }
        }

        /* Add ident */
    }
    else
    {
        ErrorTracer(MEMORY_ALLOCATION_ERROR, 0,0);
    }
}
/*
Component Function: inline void Parser::BinaryTree::AssignChildNodeNull(Tnode *Node, std::size_t NodePosition)
Arguments:  Parent node, position of child node.
Returns: None
Version : 0.1 Initial version.
Description:
Initializes childnodes to nullptr
Notes:
*/
inline void Parser::BinaryTree::AssignChildNodeNull(Tnode *Node, std::size_t NodePosition)
{

    if (NodePosition == NODE_POSITION_LEFT_SUBTREE)
    {
        Node->LeftChild=nullptr;
    }
    else if (NodePosition== NODE_POSITION_RIGHT_SUBTREE)
    {
        Node->RightChild=nullptr;
    }
}

/*
Component Function: void Parser::BinaryTree::PrintNode(const Tnode* Node)
Arguments:  Reference to Node
Returns: true or not
Description:  Displays data in node.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void Parser::BinaryTree::PrintNode(const Tnode* Node)
{
    if (Node)
    {
        if (Node->Token.Type == Lexer::TokenType::op || Node->Token.Type == Lexer::TokenType::numericliteral || Node->Token.Type == Lexer::TokenType::identifier)
        {
            Lexer::PrintToken(Node->Token);
        }

    }
    else
    {
        //std::cout << "Node is nullptr" << std::endl;
    }
}

/*
Component Function: void Parser::BinaryTree::PrintTreeNode(const Tnode& Node)
Arguments:  Reference to Node
Returns: true or not
Description:  Displays data in node.
Version : 0.1
Notes:
This function would be used while constructing Expression tree.
 */
void Parser::BinaryTree::PrintTreeNode(const Tnode& Node)
{
    if (Node.Token.Type == Lexer::TokenType::op || Node.Token.Type == Lexer::TokenType::numericliteral || Node.Token.Type == Lexer::TokenType::identifier)
    {
        Lexer::PrintToken(Node.Token);
    }
}

/*
 Component Function: void Parser::BinaryTree::ClearNode(Tnode &Node)
 Arguments:  Reference to node
 returns: None
 Description:
 Clears node
 Version : 0.1
 */
void Parser::BinaryTree::ClearNode(Tnode &Node)
{
    Node.Token.Type = Lexer::TokenType::Emptytoken;
    // Delete memory here?
    if (Node.Token.Data.u.cString)
    {
        delete[] Node.Token.Data.u.cString;
    }

    Node.LeftChild = nullptr;
    Node.RightChild = nullptr;
    Node.pParent = nullptr;
}

/*
Component Function: void  Parser::BinaryTree::ConstructBinaryExpressionTree(void)
Arguments: None
Returns : None
Description:
In this function we convert a infix expression to postfix.Use postfix notation string to construct binary expression tree.
Version and bug history:
0.1 Initial version.

Notes:
To do:
Track memory allocated for char strings from lexer to parseTokenStream() functions and to construction of binary tree, its destruction.

Known issues:
The function seems to be causing bad_alloc error with identifiers in expressions.
Terminate cause recursively is another error.
This function or another dependency function is causing a crash without any error during TreeStack processsing.
Sometimes it and its dependency functions are giving incorrect results in calculations, a value as 1.79607e-307 or other.
Identifers if used are giving a unitialized ident used error in expressions.

*/
void  Parser::BinaryTree::ConstructBinaryExpressionTree(void)
{
    /* Construct a tree by entering tokens */
    Lexer::Token_tag Token;
    //Tnode *Root;
    Tnode *aNode = nullptr;
    std::size_t OpType = NOTOP;
    //system("pause");
    while ((!isPostfixStackEmpty()) /*&& (!CheckforErrors()) */)
    {
        PopTokenPostfixStack(Token);
        aNode = CreateNodeFromToken(Token);
        if (aNode)
        {
            //PrintNode(aNode);
            //std::cout << "aNode -Node created from token is at " << aNode << std::endl;
            // Write code to attach operands to operators. Operators are inserted properly, operands should be done too
            // Pointers or actual copies used to copy nodes and build a tree should not be erased while building the tree.
            // Copy nodes by value into Tree stack and then build a tree by using their LeftChild and RightChild pointers by allocating
            // memory for them

            if (aNode->Token.Type == Lexer::TokenType::op)
            {
                OpType = Lexer::DetectOpToken(aNode->Token);
                //Lexer::PrintToken(aNode->Token);
                /* Use if else instead of switch case as case labels have same value */
                if ((OpType == OPUNARYMINUS || OpType == OPSIN || OpType == OPCOS ||\
                        OpType == OPTAN || OpType == OPSEC || OpType == OPCOT || OpType == OPCOSEC || OpType == OPSQRT  || OpType == OPLOG || \
                        OpType == OPUNARYPLUS))
                {

                    Tnode *OperandOnePtr = new Tnode;
                    if (OperandOnePtr)
                    {
                        if (!isTreeStackEmpty())
                        {
                            Parser::BinaryTree::ClearNode(*OperandOnePtr);
                            if (PopNode(OperandOnePtr))
                            {
                                /* Check Token type here and print error if wrong type */
                                Parser::BinaryTree::InsertNode(NODE_POSITION_LEFT_SUBTREE, aNode, OperandOnePtr);
                                aNode->RightChild = nullptr;
                                //PushNode(*aNode);
                                PushNode(*aNode);
                                //PrintNode(aNode);
                                //std::cout << "aNode -Node created from token is at " << aNode << std::endl;
                            }
                            else
                            {
                                ErrorTracer(MISSING_OPERAND_OR_IDENT, 0, 0);
                            }
                        }
                        else
                        {
                            delete OperandOnePtr;
                            ErrorTracer(MISSING_OPERAND_OR_IDENT, 0, 0);
                        }
                    }
                    else
                    {
                        ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
                    }

                }
                else
                {
                    Tnode *OperandOnePtr = new Tnode;
                    Tnode *OperandTwoPtr = new Tnode;
                    if ((OperandOnePtr )&& (OperandTwoPtr))
                    {
                        Parser::BinaryTree::ClearNode(*OperandOnePtr);
                        if (!isTreeStackEmpty())
                        {
                            /* Check Token type here and print error if wrong type */
                            PopNode(OperandOnePtr);
                            Parser::BinaryTree::ClearNode(*OperandTwoPtr);
                            //std::cout << "OperandOnePtr is at " << OperandOnePtr << std::endl;
                            if (!isTreeStackEmpty())
                            {
                                /* Check Token type here and print error if wrong type */
                                PopNode(OperandTwoPtr);
                                Parser::BinaryTree::InsertNode(NODE_POSITION_LEFT_SUBTREE, aNode, OperandTwoPtr);
                            }
                            else
                            {
                                delete OperandTwoPtr;
                                ErrorTracer(MISSING_OPERAND_OR_IDENT, 0, 0);
                            }
                            Parser::BinaryTree::InsertNode(NODE_POSITION_RIGHT_SUBTREE, aNode, OperandOnePtr);
                            PushNode(*aNode);
                            //PrintNode(aNode);
                            //std::cout << "aNode -Node created from token is at " << aNode << std::endl;
                        }
                        else
                        {
                            /* Check lifetime of these pointers before deletion */
                            delete OperandOnePtr;
                            ErrorTracer(MISSING_OPERAND_OR_IDENT, 0, 0);
                        }
                    }
                    else
                    {
                        ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
                    }

                }
            }
            else  if (aNode->Token.Type == Lexer::TokenType::identifier)
            {
                /* Look up ident value from symbol table if its already initialized */
                if (CurrentScope->LookupSymbolName(aNode->Token.name))
                {

                    //std::cout << "Found identifier symbol in ConstructBinaryExpresssionTree()  " << aNode->Token.name << std::endl;
                    SymbolData TempData=CurrentScope->GetSymbolData(aNode->Token.name);
                    if (TempData.pEntry->GetData().isDataInitialized)
                    {
                        aNode->Token.Data=TempData.pEntry->GetData();
                        
                        /*
                        if (aNode->Token.Data.isDataInitialized)
                        {
                            std::cout << "value of ident is " << aNode->Token.Data.u.doubleValue;
                        }
                        else
                        {
                            std::cout << "ident is uninitialized" << std::endl;
                        }
                        */
                    }

                }
                /* Lookup idents from symbol table */
                Parser::BinaryTree::AssignChildNodeNull(aNode, NODE_POSITION_LEFT_SUBTREE);
                Parser::BinaryTree::AssignChildNodeNull(aNode, NODE_POSITION_RIGHT_SUBTREE);
                // Copy node into into TreeStack.
                PushNode(*aNode);
                //PrintNode(aNode);
                //std::cout << "aNode -Node created from token is at " << aNode << std::endl;
            }
            else  if (aNode->Token.Type == Lexer::TokenType::numericliteral)
            {
                Parser::BinaryTree::AssignChildNodeNull(aNode, NODE_POSITION_LEFT_SUBTREE);
                Parser::BinaryTree::AssignChildNodeNull(aNode, NODE_POSITION_RIGHT_SUBTREE);
                // Copy node into into TreeStack.
                PushNode(*aNode);
                //PrintNode(aNode);
                //std::cout << "aNode -Node created from token is at " << aNode << std::endl;
            }
        }
        else
        {
            ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
        }
    }
    if ((!isTreeStackEmpty()) && (!CheckforErrors()))
    {
        PopNode(Root);
        Root->pParent = nullptr;
    }
    else
    {
        PrintErrors();
    }
}
/*
Component Function: Tnode*   Parser::BinaryTree::CreateNodeFromToken(const Token_tag& Token)
Arguments: Token from postfix stack
Returns: Pointer to Tree node created.
Version :
0.1 Initial version
Description:
Initializes a node with operand or operator type data.
Notes:
To do:
*/
Tnode*  Parser::BinaryTree::CreateNodeFromToken(const Lexer::Token_tag& Token)
{
    Tnode * NewNode = new Tnode;
    if (NewNode != nullptr)
    {
        NewNode->LineNumber = Token.LineNumber;
        NewNode->ColumnNumber = Token.ColumnNumber;
        switch (Token.Type)
        {

        case  Lexer::TokenType::numericliteral:
            NewNode->Token.Type =Lexer::TokenType::numericliteral;
            NewNode->Token.Data.u.doubleValue = Token.Data.u.doubleValue;
            break;
        case Lexer::TokenType::identifier:
            NewNode->Token.Type =Lexer::TokenType::identifier;
            NewNode->Token.name=Token.name;
            NewNode->Token.Data.u.doubleValue = Token.Data.u.doubleValue;
            break;
        case Lexer::TokenType::op:
            Parser::BinaryTree::AssignNodeOperator(NewNode, Token);
            break;
        /* */
        case Lexer::TokenType::separator_leftParen:
            /* Assign new memory ? */
            NewNode->Token.Type = Lexer::TokenType::separator_leftParen;
            NewNode->Token.Data.u.cString = Token.Data.u.cString;
            break;
        case Lexer::TokenType::separator_rightParen:
            /* Assign new memory ? */
            NewNode->Token.Type = Lexer::TokenType::separator_rightParen;
            NewNode->Token.Data.u.cString = Token.Data.u.cString;
            break;
        default:
            NewNode->Token.Type = Lexer::TokenType::error;
            NewNode->Token.Data.u.cString = Token.Data.u.cString;
            break;
        }
    }
    else
    {
        ErrorTracer(MEMORY_ALLOCATION_ERROR, Token.LineNumber, Token.ColumnNumber);
    }
    // Check if this pointer can be returned safely.
    return NewNode;
}

/*
Component Function: void Parser::BinaryTree::EvaluateExpression(void)
Arguments: Syntax Tree
Returns : None
Description:
This function evaluates a Tree.
Version and bug history:
0.1 Initial version.
Notes:
Function being designed.
 */
void Parser::BinaryTree::EvaluateExpression(void)
{
    Lexer::Token_tag val;

    if ((Root != nullptr))
    {
        //std::cout << "Before calling evaluate expression tree" << std::endl;
        val = EvaluateExpressionTree(Root);
        if (!CheckforErrors())
        {
            if(!equaltoopdetected)
            {
            	if (val.Data.isDataInitialized)
            	{
            		std::cout << val.Data.u.doubleValue << std::endl;            		
				}
                else
                {
                	ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
                	//std::cout << "Symbol: " << val.name <<" not found\n";
				}
            }
            equaltoopdetected = false;
            Parser::BinaryTree::ClearNode(*Root);
            //std::cout << "Type is double type" << std::endl;
            //std::cout << "Value computed is " << val.u.doubleValue << std::endl;
        }
    }
}

/*
Component Function: Lexer::Token_tag Parser::BinaryTree::EvaluateExpressionTree(Tnode *Tree )
Arguments: A abstract syntax tree with its root as argument
Returns : Token
Version : 0.1

Reference:
Aho  and Ullman- Foundations of computer science.

Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
The requirement for this function to work is that the binary expression tree should be in correct order and all variant data types
have their type declared.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed. Write clear code.
Write checks for NULL nodes at leaf nodes. This function is being constructed!
To do:

Evaluate expressions as:
sqrt(loge((3+sin(5.6))/7+(2*8)/cos(9.1-1)))

Function being designed.

*/
Lexer::Token_tag Parser::BinaryTree::EvaluateExpressionTree(Tnode *Tree)
{
    //std::cout  <<"Evaluate expression tree called " << std::endl;
    Lexer::Token_tag TokenLeft,TokenRight,Token;

    if (!CheckforErrors())
    {
        // Detect single operand expressions  as 1 , a, 2
        /* If node is a leaf or operand */
        if ( (Tree->Token.Type == Lexer::TokenType::numericliteral || Tree->Token.Type== Lexer::TokenType::identifier)  )
        {
            //std::cout <<"Node is operand" << std::endl;
            Token= Tree->Token;
        }
        /* Unary minus works the same as functions */
        else if(Tree->Token.Type  ==  Lexer::TokenType::op)
        {
            //std::cout <<"Node is operator" << std::endl;
            if ((strcmp( "_", Lexer::DetectOpstring(Tree->Token)) ==0)|| (strcmp( "sin", Lexer::DetectOpstring(Tree->Token)) ==0)||\
                    (strcmp( "cos", Lexer::DetectOpstring(Tree->Token)) ==0) || (strcmp( "tan", Lexer::DetectOpstring(Tree->Token)) ==0) ||\
                    (strcmp( "cosec", Lexer::DetectOpstring(Tree->Token)) ==0) || (strcmp( "cot", Lexer::DetectOpstring(Tree->Token)) ==0) ||\
                    (strcmp( "loge", Lexer::DetectOpstring(Tree->Token)) ==0) ||(strcmp( "sqrt", Lexer::DetectOpstring(Tree->Token)) ==0) ||\
                    (strcmp( "_+_", Lexer::DetectOpstring(Tree->Token)) ==0))
            {
                TokenLeft = EvaluateExpressionTree(Tree->LeftChild);
                Token = ComputeArithmetic(Tree, TokenLeft, TokenRight, Token);
            }
            else
            {
                /* a=1 causes Token Left to be a, Token right to be 1, Token to be 1 */
                // Tokens should be looked up and updated  here rather than in  computearithmetic ?
                TokenLeft = EvaluateExpressionTree(Tree->LeftChild);
                TokenRight = EvaluateExpressionTree(Tree->RightChild);
                Token = ComputeArithmetic(Tree, TokenLeft, TokenRight, Token);
            }
        }

    }
    /* Return Token */
    return Token;
}

/*
Component Function: Lexer::Token_tag Parser::BinaryTree::ComputeArithmetic(Tnode *Tree, Lexer::Token_tag& TokenLeft, Lexer::Token_tag& TokenRight,Lexer::Token_tag& Token)
Arguments: Tree,
Returns : Token . Return pointer instead?
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
The requirement for this function to work is that the binary expression tree should be incorrect order.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed.
To do:
add identifier support.
Add code for identifiers for lhs and rhs.
Add look up into symbol table to get values of identifiers.
Initialize identifiers with numeric operands in expressions as
a=1
a=b+c*d+1
Known issues:

Outputs are in < >


Error 3:
Fix exponent operator ^ with expressions as 2^4

Error 4:
Unary plus is also causing a crash

Initialize idents correctly.

Error 6:
Evaluate expressions as:
sqrt(loge((3+sin(5.6))/7+(2*8)/cos(9.1-1)))
This is not evaluated correctly or causing a crash.

Any arithmetic performed with idents should not change the value of ident.

Error 7:
exit command causing crash sometimes. Keyboard entry error?


Error 9:
a=1
a
<1>
m
<1>

Any unitilalized variable m also prints 1 unless it is initialized.

The above errors 1 to 8 seem correctable errors which are fixed by configuring the code below and in tree functions correctly.
*/
Lexer::Token_tag  Parser::BinaryTree::ComputeArithmetic(Tnode *Tree, Lexer::Token_tag& TokenLeft, Lexer::Token_tag& TokenRight,Lexer::Token_tag& Token)
{
    /* Type cast types of operands */
    //Detect operators.
    /* Do arithmetic operation on operands.*/
    /* Use variant instead of double. This would be useful for updating symbols.*/
    BOOL TokenLeftFound=false, TokenRightFound=false;
    /* These constants are same for some values. Change that */
    /* Detect op */


    if (strcmp( "_", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=-TokenLeft.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue=-TokenLeft.Data.u.doubleValue;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "sin", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
                    Token.Data.u.doubleValue= sin(TokenLeft.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }

            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            Token.Type=Lexer::TokenType::numericliteral;
            TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
            Token.Data.u.doubleValue= sin(TokenLeft.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }

    }
    else if (strcmp( "cos", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
                    Token.Data.u.doubleValue= cos(TokenLeft.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }

            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            Token.Type=Lexer::TokenType::numericliteral;
            TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
            Token.Data.u.doubleValue= cos(TokenLeft.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }

    }
    else if (strcmp( "tan", Lexer::DetectOpstring(Tree->Token)) ==0)
    {

        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
                    Token.Data.u.doubleValue= tan(TokenLeft.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }

            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            Token.Type=Lexer::TokenType::numericliteral;
            TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
            Token.Data.u.doubleValue= tan(TokenLeft.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }

    }
    else if (strcmp( "sec", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
                    Token.Data.u.doubleValue= 1/cos(TokenLeft.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }

            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            Token.Type=Lexer::TokenType::numericliteral;
            TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
            Token.Data.u.doubleValue= 1/cos(TokenLeft.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "cosec", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
                    Token.Data.u.doubleValue= 1/sin(TokenLeft.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }

            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            Token.Type=Lexer::TokenType::numericliteral;
            TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
            Token.Data.u.doubleValue= 1/sin(TokenLeft.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "cot", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
                    Token.Data.u.doubleValue= 1/tan(TokenLeft.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }

            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            Token.Type=Lexer::TokenType::numericliteral;
            TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * PI/180;;
            Token.Data.u.doubleValue= 1/tan(TokenLeft.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "sqrt", Lexer::DetectOpstring(Tree->Token)) ==0)
    {

        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= sqrt(TokenLeft.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= sqrt(TokenLeft.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "loge", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= log(TokenLeft.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= log(TokenLeft.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }


    /* Any arithmetic performed on idents will return a result as a numeric value.
    Arithmetic peformed with idents should not change value of idents.*/
    /* Type of result is changing the type of token in callee function. The token in
    host function can be ident, token here is numericliteral. */


    else if (strcmp( "-", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue-TokenRight.Data.u.doubleValue;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "/", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue / TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue / TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue / TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue / TokenRight.Data.u.doubleValue;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "+", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue + TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue + TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue + TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue + TokenRight.Data.u.doubleValue;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "*", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * TokenRight.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * TokenRight.Data.u.doubleValue;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }

    else if (strcmp( "^", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=pow(TokenLeft.Data.u.doubleValue,TokenRight.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=pow(TokenLeft.Data.u.doubleValue,TokenRight.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=pow(TokenLeft.Data.u.doubleValue,TokenRight.Data.u.doubleValue);
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue=pow(TokenLeft.Data.u.doubleValue,TokenRight.Data.u.doubleValue);
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "<", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue< TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue< TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue< TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue< TokenRight.Data.u.doubleValue) ? 1:0;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( ">", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue > TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue > TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue > TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue > TokenRight.Data.u.doubleValue) ? 1:0;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "==", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= ((TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue   <=0.0000001) ||\
                                               (TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue   <= -0.0000001)) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= ((TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue   <=0.0000001) ||\
                                               (TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue   <= -0.0000001)) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= ((TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue   <=0.0000001) ||\
                                               (TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue   <= -0.0000001)) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= ((TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue   <=0.0000001) ||\
                                       (TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue   <= -0.0000001)) ? 1:0;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "!=", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue != TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue != TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue != TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue != TokenRight.Data.u.doubleValue) ? 1:0;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "<=", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue <= TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue <= TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue <= TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue <= TokenRight.Data.u.doubleValue) ? 1:0;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }

    else if (strcmp( ">=", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue >= TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue >= TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue >= TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue >= TokenRight.Data.u.doubleValue) ? 1:0;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "||", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue || TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue || TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue || TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue || TokenRight.Data.u.doubleValue) ? 1:0;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else if (strcmp( "&&", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {


            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue && TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue && TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else  if ( TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if (TokenRight.Data.isDataInitialized )
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue && TokenRight.Data.u.doubleValue) ? 1:0;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue= (TokenLeft.Data.u.doubleValue && TokenRight.Data.u.doubleValue) ? 1:0;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }


    /*
    update symbol on lhs for any = operator token.
     if any op on lhs is numeric, signal error.
     If it is ident, assign rhs to lhs
     Use Scopes->UpdateSymbol() function
     Add errors for initialization
     */
    else if (strcmp( "=", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        //system("pause");
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenRightFound)
            {
                if  (TokenRight.Data.isDataInitialized)
                {
                    TokenLeft.Data=TokenRight.Data;
                    TokenLeft.Data.Type=IDENT;
                    TokenRight.Data.Type=IDENT;
                    CurrentScope->UpdateSymbol(TokenLeft.name,TokenRight.Data);
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            //TokenLeft.Data=TokenRight.Data;
            //TokenLeft.Data.isDataInitialized=true;
            TokenLeft.Data.isDataInitialized=true;
            TokenLeft.Data.Type=IDENT;
            TokenRight.Data.Type=DOUBLETYPE;
            TokenRight.Data.isDataInitialized=true;
            CurrentScope->UpdateSymbol(TokenLeft.name,TokenRight.Data);
            
            /*
            if (TokenLeft.Type==Lexer::TokenType::identifier)
                std::cout << "Token left is ident " << TokenLeft.name << std::endl;
            
			*/
			//Lexer::PrintToken(TokenLeft);
			
			/*
            if (TokenRight.Type==Lexer::TokenType::numericliteral)
                std::cout << "Token right is numeric literal " << std::endl;
            */
            //Lexer::PrintToken(TokenRight);
            /*
            if (TokenRight.Data.isDataInitialized)
            {
                std::cout << "Data in Numerical token is intialized" << std::endl;
            }
            */

        }
        else if (TokenLeft.Type== Lexer::TokenType::numericliteral)
        {
            ErrorTracer(CANNOTASSIGN_VALUETO_NUMERICOPERAND,0,0);
        }
    }
    else if (strcmp( "/=", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {

                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue/TokenRight.Data.u.doubleValue;

                    Token=TokenLeft;
                    Token.Data.Type=DOUBLETYPE;
                    Token.Data.isDataInitialized=true;
                    CurrentScope->UpdateSymbol(TokenLeft.name,Token.Data);
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue/TokenRight.Data.u.doubleValue;
                    Token=TokenLeft;
                    Token.Data.Type=DOUBLETYPE;
                    Token.Data.isDataInitialized=true;
                    CurrentScope->UpdateSymbol(TokenLeft.name,TokenLeft.Data);

                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            // Error, cant assign value to numeric value.
            ErrorTracer(CANNOTASSIGN_VALUETO_NUMERICOPERAND,0,0);
        }
    }
    else if (strcmp( "*=", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {

                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue *TokenRight.Data.u.doubleValue;
                    Token=TokenLeft;
                    Token.Data.Type=DOUBLETYPE;
                    Token.Data.isDataInitialized=true;
                    CurrentScope->UpdateSymbol(TokenLeft.name,TokenLeft.Data);

                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue * TokenRight.Data.u.doubleValue;
                    CurrentScope->UpdateSymbol(TokenLeft.name,TokenLeft.Data);
                    Token=TokenLeft;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            // Error, cant assign value to numeric value.
            ErrorTracer(CANNOTASSIGN_VALUETO_NUMERICOPERAND,0,0);
        }


    }
    else if (strcmp( "-=", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {

                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue -TokenRight.Data.u.doubleValue;
                    CurrentScope->UpdateSymbol(TokenLeft.name,TokenLeft.Data);
                    Token=TokenLeft;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue - TokenRight.Data.u.doubleValue;
                    CurrentScope->UpdateSymbol(TokenLeft.name,TokenLeft.Data);
                    Token=TokenLeft;
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            // Error, cant assign value to numeric value.
            ErrorTracer(CANNOTASSIGN_VALUETO_NUMERICOPERAND,0,0);
        }


    }
    else if (strcmp( "+=", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier && TokenRight.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            TokenRightFound=Parser::LookupNameinSymbolTable(TokenRight);
            if (TokenLeftFound && TokenRightFound)
            {
                if (TokenLeft.Data.isDataInitialized && TokenRight.Data.isDataInitialized)
                {

                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue +TokenRight.Data.u.doubleValue;
                    Token=TokenLeft;
                    Token.Data.Type=DOUBLETYPE;
                    Token.Data.isDataInitialized=true;
                    CurrentScope->UpdateSymbol(TokenLeft.name,TokenLeft.Data);
                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::identifier  && TokenRight.Type==Lexer::TokenType::numericliteral)
        {

            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized )
                {
                    TokenLeft.Data.u.doubleValue=TokenLeft.Data.u.doubleValue + TokenRight.Data.u.doubleValue;
                    Token=TokenLeft;
                    Token.Data.Type=DOUBLETYPE;
                    Token.Data.isDataInitialized=true;
                    CurrentScope->UpdateSymbol(TokenLeft.name,TokenLeft.Data);

                }
                else
                {
                    ErrorTracer(UNINITIALIZED_IDENT_USEDINEXPRESSION,0,0);
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }
        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {
            // Error, cant assign value to numeric value.
            ErrorTracer(CANNOTASSIGN_VALUETO_NUMERICOPERAND,0,0);
        }
    }
    // op unary plus
    else if (strcmp( "_+_", Lexer::DetectOpstring(Tree->Token)) ==0)
    {
        // Code change here
        if (TokenLeft.Type==Lexer::TokenType::identifier)
        {
            TokenLeftFound=Parser::LookupNameinSymbolTable(TokenLeft);
            if (TokenLeftFound)
            {
                if (TokenLeft.Data.isDataInitialized)
                {
                    Token.Type=Lexer::TokenType::numericliteral;
                    Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue;
                    Token.Data.isDataInitialized=true;
                    Token.Data.Type=DOUBLETYPE;
                }
            }
            else
            {
                ErrorTracer(IDENTIFIER_NOTFOUND,0,0);
            }
        }

        else if (TokenLeft.Type==Lexer::TokenType::numericliteral)
        {

            Token.Type=Lexer::TokenType::numericliteral;
            Token.Data.u.doubleValue=TokenLeft.Data.u.doubleValue;
            Token.Data.isDataInitialized=true;
            Token.Data.Type=DOUBLETYPE;
        }
    }
    else
    {

    }

    return Token;
}

/*
Component Function: void Parser::BinaryTree::DeleteBinaryTree(Tnode *Tree)
Arguments: Root node of a binary tree.
Returns : None.
Description:
This function deletes nodes in a Binary expression tree bottom up. First the function would delete all leaf nodes in the left subtree
and then it would delete leaf nodes in the right subtree.Then operator nodes are deleted. The root is deleted as well.
Version and bug history:
0.1 Initial version.
0.2 Working debug version displaying operands and operators.
Importance:
Very important function in deallocating memory in binary trees and AST.Make this function scalable for use on AST.

Notes:
Function being designed.

 */
void Parser::BinaryTree::DeleteBinaryTree(Tnode *Tree)
{

    if (Tree)
    {
        std::size_t OpType = NOTOP;
        if (Tree->Token.Type == Lexer::TokenType::op)
        {
            //std::string OpString (Tree->Op);
            OpType= Lexer::DetectOpToken(Tree->Token);
        }
        Tnode *Temp = nullptr;
        //std::cout << "In DeleteBinaryTree() function"  << std::endl;
        if ( Tree->Token.Type == Lexer::TokenType::numericliteral || Tree->Token.Type== Lexer::TokenType::identifier)
        {
            //#if(0)
            if (Tree->LeftChild == nullptr && Tree->RightChild == nullptr && Tree->pParent!= nullptr)
            {
                //std::cout << "Detected leaf node: " << std::endl;
                // Detect if leaf is left child or right child,make sure there is no dangling leaf node which is not deleted.

                if (Tree == Tree->pParent->LeftChild)
                {
                    //std::cout << "Deleting Left leaf operand node: ";
                    // A left leaf
                    Temp = Tree->pParent;
                    Temp->LeftChild = nullptr;


                    delete Tree;
                    Tree=nullptr;

                }
                else if (Tree == Tree->pParent->RightChild)
                {
                    // A right leaf
                    //std::cout << "Deleting Right leaf operand node: ";
                    Temp = Tree->pParent;
                    Temp->RightChild = nullptr;

                    delete Tree;
                    Tree=nullptr;
                }

            }
            else if (Tree->LeftChild == nullptr && Tree->RightChild == nullptr && Tree->pParent== nullptr)
            {
                // Detect if operand is only node in tree.
                //std::cout << "Deleting Root operand node: ";
                // A Root operand

                delete Tree;
                Tree=nullptr;
            }
            //#endif
        }
        /* if Node is unary minus with one left child */
        else if (Tree->Token.Type == Lexer::TokenType::op )
        {

            if ((OpType == OPUNARYMINUS || OpType == OPSIN || OpType == OPCOS ||\
                    OpType == OPTAN || OpType == OPSEC || OpType == OPCOT || OpType == OPCOSEC || OpType == OPSQRT  || OpType == OPLOG \
                    || OpType == OPUNARYPLUS))
            {
                // Detect if op is left child or right child,make sure there is no dangling op node which is not deleted.
                if (Tree->pParent != nullptr and Tree == Tree->pParent->LeftChild)
                {
                    // A left child op node
                    //std::cout << "Deleting Left leaf unary minus operator node: " << Tree->Op << std::endl;
                    Temp = Tree->pParent;
                    Temp->LeftChild = nullptr;
                    //free(Tree->Data.u.cString);


                    delete Tree;
                    Tree=nullptr;
                }
                else if (Tree->pParent != nullptr && Tree == Tree->pParent->RightChild)
                {
                    // A Right child op node.
                    //std::cout << "Deleting Right leaf unary minus operator node: " << Tree->Op << std::endl;
                    Temp = Tree->pParent;
                    Temp->RightChild = nullptr;
                    //free(Tree->Data.u.cString);
                    delete Tree;
                    Tree=nullptr;
                }
                else if (Tree->pParent == nullptr && Tree->LeftChild != nullptr && Tree->RightChild == nullptr)
                {
                    //std::cout << "Detected Root operator node: " << Tree->Op << std::endl;
                }

                Parser::BinaryTree::DeleteBinaryTree(Tree->LeftChild);

            }
            /* Change this condition */
            else if ((OpType != OPUNARYMINUS && OpType != OPSIN && OpType != OPCOS\
                      && OpType != OPTAN && OpType != OPSEC && OpType != OPCOT && OpType != OPCOSEC && OpType != OPSQRT  && OpType != OPLOG\
                      && OpType != OPUNARYPLUS ))
            {
                //#if(0)
                // Detect if op is left child or right child,make sure there is no dangling op node which is not deleted.
                //#if(0)
                if (Tree->pParent != nullptr and Tree == Tree->pParent->LeftChild)
                {
                    // A left child op node
                    //std::cout << "Deleting Left leaf operator node: " << Tree->Op << std::endl;

                    Temp = Tree->pParent;
                    Temp->LeftChild = nullptr;
                    //free(Tree->Data.u.cString);
                    delete Tree;
                    Tree=nullptr;

                }//#endif
                // Detect if op is left child or right child,make sure there is no dangling op node which is not deleted.
                else if (Tree->pParent != nullptr && Tree == Tree->pParent->RightChild)
                {
                    // A Right child op node.
                    //std::cout << "Deleting Right leaf operator node: " << Tree->Op << std::endl;
                    Temp = Tree->pParent;
                    Temp->RightChild = nullptr;
                    //free(Tree->Data.u.cString);
                    delete Tree;
                    Tree=nullptr;
                }
                else if (Tree->pParent == nullptr && Tree->LeftChild != nullptr && Tree->RightChild != nullptr)
                {

                    //std::cout << "Detected Root operator node: " << Tree->Op << std::endl;
                }
                Parser::BinaryTree::DeleteBinaryTree(Tree->LeftChild);
                Parser::BinaryTree::DeleteBinaryTree(Tree->RightChild);
            }


            else if (Tree->pParent == nullptr && Tree->LeftChild != nullptr && Tree->RightChild == nullptr && (Tree))
            {
                //Deleting Root node which is unary minus or a function such as sin,cos
                //std::cout << "Deleting Root node which is unary minus or a function such as sin,cos" << std::endl;
                //free(Tree->Data.u.cString);
                /*
                delete Tree;
                Tree = nullptr;
                */
                if (Tree->LeftChild )
                {
                    delete Tree->LeftChild;
                    Tree->LeftChild=nullptr;
                }
                delete Tree;
                Tree = nullptr;
            }
            else if ( Tree->pParent == nullptr && Tree->LeftChild == nullptr && Tree->RightChild == nullptr && (Tree))
            {
                //std::cout << "Deleting Root node " << std::endl;
                Tree->Token.Data.u.doubleValue=0;
                delete Tree;
                Tree = nullptr;
            }
        }

    }
    else
    {
        //std::cout << "Tree empty" << std::endl;
    }


}
/*
Component Function:  inline void Parser::BinaryTree::AssignNodeOperator(Tnode *Node,const Lexer::Token_tag& Token)
Arguments:  None
Returns: None
Version : 0.1 Initial version.
Description:
Initializes a node as operator.
Notes:
*/

inline void Parser::BinaryTree::AssignNodeOperator(Tnode *Node,const Lexer::Token_tag& Token)
{
    if (Node)
    {
        /* Assign new memory ? */
        Node->Token.Type=Token.Type;
        Memcopy(Node->Token.Data.u.cString,Token.Data.u.cString);
    }
    else
    {
        ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
    }
}
