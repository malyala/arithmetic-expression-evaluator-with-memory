/*
Name :  Calc.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module initliazes parser
Notes:

Bug and Version history:
0.1 Initial version
Notes: construct syntax tree or binary expression tree.
The parser would get each token and construct a tree. It would either add each token into a tree or evaluate that tree which is being
constructed.This is based on token newline and if any '=' operator was present in the expression.

If '=' is detected in expression, then symbol is updated in symbol table or else the expression is evaluated after Newline token.

Operator table in decreasing order of precedence
-------------------------------------------------
(sin,cos,tan,sec,cosec,cot,loge, pow) -O1
								   ~  -O2
								   !  -O3
						  unary minus -O4
						   unary plus -O5
							  (* / %) -O6
								 (+-) -O7
							( << >> ) -O8
						  (< <= > >= )-O9
							( ==, != )-O10
								  &   -O11
								 xor  -O12
								  |   -O13
								 &&   -O14
								 ||   -O15

								  =	  -O16      Direct assignment (provided by default for C++ classes)
							+=   -=	  -O17      Compound assignment by sum and difference
						  *=   /=   %= -O18	    Compound assignment by product, quotient, and remainder
							 <<=   >>= -O19	    Compound assignment by bitwise left shift and right shift
						   &=   ^=  |= -O20

To do:

Add operators:
								  =	  -O16      Direct assignment (provided by default for C++ classes)
							+=   -=	  -O17      Compound assignment by sum and difference
						  *=   /=   %= -O18	    Compound assignment by product, quotient, and remainder
							 <<=   >>= -O19	    Compound assignment by bitwise left shift and right shift
						   &=   ^=  |= -O20



Notes:


The program executes as below with outputs or internal operations under << >>. Everything else is input.

    a=1
	a
	<<1>>
	b=3
	c=4
	d=(a*123+b/123+c*8)/(c-0.2)
	d
    << Prints value of d as 40.79>>

	CLEAR
	<< clears all variables >>
    
    a
    <<Error 41: Symbol(s) not found >>
    
    b
    <<Error 41: Symbol(s) not found >>
        
    a=11
	a
	11
	b=31
	c=49
	(a*123+b/123+c*8)/(c-0.2)
    << Prints result of expression >>
    CLEAR
	<< clears all variables >>

	EXIT
	<<program exits >>

To do:


Coding log:
18-06-19  Code adapted from a previous baseline. Project compiles in G++ under -std=c++14
19-06-20  Added components Lexer::isCommand() in Lexer::Tokenprocessor()  , Datastructures::isAlpha() Datastructures::tolower() functions. 

          Moved strlength() function into namespace Datastructures.
26-12-20 Changed Lexer::getNextToken and Lexer::TokenProcessor() functions along with Parser::constructBinaryTree and Parser::EvaluateExpression. 
         Corrected a few bugs by configuring the software. Code to be modified to add overflow and underflow detection as exceptions for arithmetic operators.   Code to be modified 
        to change ErrorTracer mechanism into C++ exceptions as an alternative for Error detection mechanism. To print all errors, 
        exceptions have to be locally thrown and caught. 
*/

#include "parser.h"


/* 
Application
*/
void ExecuteApp(void);
//#if(0)
/* Comment to test module. Uncomment to integrate */
SINT32 main(void)
{
    ExecuteApp();
    return 0;
}
//#endif

/* 
Application
*/
void ExecuteApp(void) 
{
    try
    {
        Parser::ParseandEvaluate();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
}
