# Arithmetic expression calculator with memory

This project creates a programmable arithmetic expression evaluator with ISO C++14. Component based software engineering is used to design and develop it.
It compiles with -std=c++14 option using MingW G++ compiler. See the file Buglist for list of known bugs or new features required. 

The program executes as below with outputs or internal operations under << >>. Everything else is input. 

    a=1
    a
    <<1>>
    b=3
    c=4
    d=(a*123+b/123+c*8)/(c-0.2)
    d
    << Prints value of d >>

    CLEAR
    << clears all variables >> 
    
    a
    << Error : Symbol(s) not found >>
    b
    << Error : Symbol(s) not found >>
    
    a=11
    a
    <<11>>
    b=31
    c=49
    (a*123+b/123+c*8)/(c-0.2)
    << Prints result of expression >>
    CLEAR
    << clears all variables >> 

    EXIT
    <<program exits >>
	

# Data flow diagram for Arithmetic expression evaluator
![alt text](https://bitbucket.org/malyala/arithmetic-expression-evaluator-with-memory/downloads/Dataflow_parser_calculator.png)


Copyright Amit Malyala, 2019, Made in Andhra.