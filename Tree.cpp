/*
        Name: Tree.cpp
        Copyright: Amit Malyala, 2016, All rights reserved.
        Author: Amit Malyala
        Date: 07-12-16 16:28
        Description:
        This module provides functions for creating, inserting and deleting Tree nodes in a binary expression tree.
*/

#include "Tree.h"
#include "Stack.h"
#include "Parser.h"


/* Tnode constructor */
Tnode::Tnode()
{
    LeftChild=nullptr;
    RightChild=nullptr;
    pParent=nullptr;
}

/* Tnode destructor */
Tnode::~Tnode()
{
	if (Token.name!= nullptr)
	{
		delete[] Token.name;
	}
	 
	if (Token.Data.u.cString != nullptr)
	{
		delete[] Token.Data.u.cString;
	}
}
