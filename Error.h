/*
Name :  Error.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
Description:
This module produces errors when called from different modules. These errors would be reported
to the Interpreter control program.
Notes:

Bug and revisioin history;
        Version 0.1 Initial version

*/

#ifndef ERROR_H
#define ERROR_H

#include <string>
#include <vector>
#include <iostream>
#include <cstddef>
#include "std_types.h"
#include "Datastructures.h"



// Error codes for various syntax errors.
#define MISSING_COMMENT 0x01
#define INCORRECT_DATA_DEFINITION 0x02
#define INCORRECT_DATA_DECLARATION 0x03
#define MISSING_FUNCTION_ARGS 0x04
#define INCORRECT_FUNCTION_DEFINITION 0x05
#define ILLEGAL_CHARACTER 0x06
#define MISSING_LEFT_PARENTHESIS 0x07
#define MISSING_RIGHT_PARENTHESIS 0x08
#define INVALID_IDENT_KEYWORD_USED 0x09
#define STACK_OVERFLOW 0x0A
#define MEMORY_ALLOCATION_ERROR 11
#define NUMBER_OUT_OF_RANGE 12
#define UNABLE_TO_PROCESS_LINENUMBER 13
#define MISSING_SLASH 14
#define DEALLOCATING_MEMORY_FOR_DELETED_NODE 15
#define CONSECUTIVE_MULTIPLICATION_OPERATORS 16
#define CONSECUTIVE_DIVIDE_OPERATORS 17
#define INCORRECT_ARITHMETIC 18
#define STACK_ACCESS_ERROR 19
#define MISSING_OPERAND_OR_IDENT 20
#define MISSING_OPERATOR 21
#define EMPTY_PARENTHESES 22
#define EMPTY_EXPRESSION 23
#define CONSECUTIVE_MODULUS_OPERATORS 24
#define MODULUS_OPERATOR_ON_FLOAT_NUMBERS 25
#define CONSECUTIVE_EXPONENT_OPERATORS 26
#define BITWISEARITHMETIC_ON_FLOATINGPOINTNUMBERS 27
#define NAME_TOOLONG 34
#define IDENTIFER_IS_STATIC_ACCESSED_IN_OTHERTU 35
#define MEMORY_ALLCATION_ERROR 36
#define IDENTIFIER_STARTCHAR_ERROR 37
#define SYNTAX_ERROR 38
#define INCORRECTNUMERIC_VALUE 39
#define MISSING_PARENTHESES 40
#define IDENTIFIER_NOTFOUND 41
#define CANNOTASSIGN_VALUETO_NUMERICOPERAND 42
#define UNINITIALIZED_IDENT_USEDINEXPRESSION 43



/* Error entry data type */
struct ErrorEntry
{
    std::string Error;
    std::size_t ErrorType;
    std::size_t linenumber;
    std::size_t columnnumber;
};

// Other error codes
#define NO_ERRORS 0x0
#define COMPILATION_ERRORS 0x1
#define COMMENTSPROCESSINGERRORS 2

/*
Component Function: void ErrorTracer ( std::size_t ErrorCode, std::size_t LineNumber,std::size_t ColumnNumber)
Arguments:  Error from module string and Error code
returns: None
Description:
Adds Error from module with error code to a vector of strings which contains total errors ever captured.
Version : 0.2
*/
void ErrorTracer ( std::size_t ErrorCode, std::size_t LineNumber,std::size_t ColumnNumber);

/*
Component Function: std::size_t CheckforErrors(void)
Arguments:  None
returns: None
Description:
Checks if errors are present and returns errors present or not present.
Version : 0.1
 */
std::size_t CheckforErrors(void);
/*
Component Function: void PrintErrors(void)
Arguments:  None
returns: None
Description:
Displays errors present on a console.
Version : 0.1
 */
void PrintErrors(void);

/*
Component Function:  void InitErrorTracer(void)
Arguments:  None
returns: None
Description:
Initialize Error tracer module.
Version : 0.1
*/
void InitErrorTracer(void);
#endif
