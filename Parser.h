/*
Name :  Parser.h
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module initliazes symbol Table.
Notes:

The syntax tree would have the following:
binary Operator node with two operands
unary oprator node with Single operand.
identifier Operands in either one or two branches for each operator.
Numeric literal Operands in either one or two branches for each operator.
One numeric literal and one identifier branch for each operator.

A operator node is parent node.

Bug and revision history;
0.1 Initial version
0.2 Added line numbers at which Errors occur
0.3 Changed some function parameters to const data type &
*/

#ifndef PARSER_H
#define PARSER_H


#ifndef PI
#define PI 3.1415
#endif

/* Define labels for nodes. Use type name of lexer token instead.*/
#define NODE_LABEL_OPERATOR 0x00
#define NODE_LABEL_OPERAND 0x01
#define NODE_LABEL_IDENTIFIER 0x02

#define NODE_LABEL_LEFT
#define NODE_LABEL_RIGHTPAREN 0x06
/* Some more labels to be added */
#define NODE_LABEL_LEFTBRACE 0x07
#define NODE_LABEL_RIGHTBRACE 0x08
#define NODE_LABEL_CONDITION 0x03
#define NODE_LABEL_BODY 0x04
#define NODE_LABEL_nullptr 0x09
#define NODE_NOT_INITIALIZED 12
/* Labels for binary trees */
#define NODE_POSITION_LEFT_SUBTREE 10
#define NODE_POSITION_RIGHT_SUBTREE 11
#include "std_types.h"
#include "Error.h"

#include "Lexer.h"
#include "stack.h"

#include <memory>



namespace Parser
{

/*
 Component Function: void ParseandEvaluate(void)
 Arguments: None
 Returns: None
 Description:
  Initializes the parser
  Version : 0.1
*/
void ParseandEvaluate(void);

/*
Component Function: void  ParseTokenstream(Lexer::Token_tag& Token)
Arguments: Token stream
Returns: None
Version : 0.1 Initial version.
Description:
Notes:
To do:
*/
void  ParseTokenstream(Lexer::Token_tag& Token);


/* Lookup token in symbol table */
BOOL LookupNameinSymbolTable(Lexer::Token_tag& Token);
/*
Component Function: ParseOperatorandOperand(Lexer::Token_tag& Temp,Lexer::Token_tag& OpTemp,Lexer::Token_tag& CurrentToken,UITN32& CurrentOp)
Arguments: Temp and OP temp
Returns: None
Description:  Modifies RPN stack for each operator
Version: 0.1 Initial version
*/
void  ParseOperatorandOperand(Lexer::Token_tag& Temp,Lexer::Token_tag& OpTemp,Lexer::Token_tag& CurrentToken,std::size_t& CurrentOp);

/* Parse operator */
void DetectUnderlyingOperatorsinOpStack(Lexer::Token_tag& OpTemp,std::size_t& CurrentOp);


/* namespace interface for tree module */
namespace BinaryTree
{

/*
Component Function: void InsertNode(std::size_t NodePos,Tnode *Tree,Tnode *NewNode)
Arguments: TBD
Returns : None
Description:
This function inserts a node into a binary expression tree. A node is
inserted as a left or right node.
Version and bug history:
0.1 Initial version.
Notes: Function being designed.
*/
void InsertNode(std::size_t NodePos,Tnode *Tree,Tnode *NewNode);




/*
Component Function: void PrintNode(const Tnode* Node)
Arguments:  Reference to Node
Returns: None
Description:  Displays data in node.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void PrintNode(const Tnode* Node);
/*
Component Function: Lexer::Token_tag ComputeArithmetic(Tnode *Tree, Lexer::Token_tag& TokenLeft, Lexer::Token_tag& TokenRight,Lexer::Token_tag& Token)
Arguments: Tree,
Returns : Token . Return pointer instead?
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
The requirement for this function to work is that the binary expression tree should be incorrect order.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed.
To do:

Known issues:
None

 */
Lexer::Token_tag ComputeArithmetic(Tnode *Tree, Lexer::Token_tag& TokenLeft, Lexer::Token_tag& TokenRight,Lexer::Token_tag& Token);
/*
Component Function: void PrintTreeNode(const Tnode& Node)
Arguments:  Reference to Node
Returns: true or not
Description:  Displays data in node.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void PrintTreeNode(const Tnode& Node);
/*
Component Function: void ClearNode(Tnode &Node)
Arguments:  Reference to node
Returns: None
Description:
Clears at token to read next token
Version : 0.1
*/
void ClearNode(Tnode &Node);


/*
Component Function: inline void AssignChildNodeNull(Tnode *Node, std::size_t NodePosition)
Arguments:  Parent node, position of child node.
Returns: None
Version : 0.1 Initial version.
Description:
Initializes childnodes to nullptr
Notes:
*/
inline void AssignChildNodeNull(Tnode *Node, std::size_t NodePosition);

/*
Component Function: void  ConstructBinaryExpressionTree(void)
Arguments: None
Returns : None
Description:
In this function we convert a infix expression to postfix.Use postfix notation string to construct binary expression tree.
Initially the parser will construct using a postfix notation stream of tokens, after this is done, the parser will use lexer output stream
to construct AST.The important thing here is to enable expression evaluation in the midst of statements and compound statements which the AST will
facilitate.The AST will evaluate expression..A postfix evaluator function is only for expression of arithmetic operands and operators.
Version and bug history:
0.1 Initial version.
Notes:
To do:
Track memory allocated for char strings from lexer to parseTokenStream() functions and to construction or binary tree, destruction.
*/
void  ConstructBinaryExpressionTree(void);





/*
Component Function: void DeleteBinaryTree(Tnode *Tree)
Arguments: Root node of a binary tree.
Returns : None.
Description:
This function deletes nodes in a Binary expression tree bottom up. First the function would delete all leaf nodes in the left subtree
and then it would delete leaf nodes in the right subtree.Then operator nodes are deleted. The root is deleted as well.
Version and bug history:
0.1 Initial version.
0.2 Working debug version displaying operands and operators.
Importance:
Very important function in deallocating memory in binary trees and AST.Make this function scalable for use on AST.

Notes:
Function being designed.
*/
void DeleteBinaryTree(Tnode *Tree);

/*
Component Function: Tnode*  CreateNodeFromToken(const Token_tag& Token)
Arguments: Token from postfix stack
Returns: Pointer to Tree node created.
Version :
0.1 Initial version
Description:
Initializes a node with operand or operator type data.
Notes:
To do:
*/

Tnode* CreateNodeFromToken(const Lexer::Token_tag& Token);


/*
Component Function: void EvaluateExpression(void)
Arguments: None
Returns : None
Description:
This function evaluates a Tree.
Version and bug history:
0.1 Initial version.
Notes:
 */
void EvaluateExpression(void);


/*
Component Function: Lexer::Token_tag EvaluateExpressionTree(Tnode *Tree)
Arguments: A abstract syntax tree with its root as argument
Returns : Token
Version : 0.1

Reference:
Aho  and Ullman- Foundations of computer science.

Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
The requirement for this function to work is that the binary expression tree should be in correct order and all variant data types
have their type declared.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed. Write clear code.
Write checks for NULL nodes at leaf nodes. This function is being constructed!
To do:
Initialize identifiers with numeric operands in expressions as
a=1
a=b+c*d+1


Function being designed.
*/
Lexer::Token_tag EvaluateExpressionTree(Tnode *Tree);



/*
Component Function:  inline void AssignNodeOperator(Tnode *Node,const Lexer::Token_tag& Token)
Arguments:  None
Returns: None
Version : 0.1 Initial version.
Description:
Initializes a node as operator.
Notes:
*/

inline void AssignNodeOperator(Tnode *Node,const Lexer::Token_tag& Token);
} /* namespace BinaryTree */



}


#endif // 
