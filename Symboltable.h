/*
Name :  Symboltable.h
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module contains symbol table class and its functions.
Notes:
*/
#ifndef SYMBOLTABLE
#define SYMBOLTABLE

#include "std_types.h"
#include <cstdlib>
#include "Error.h"
#include <cstring>
#include "DataStructures.h"
#include "Hash.h"
#include <memory>

/* Storage classes of identifiers */
#define STORAGE_AUTO  1
#define STORAGE_STATIC 2
#define STORAGE_GLOBAL 4
#define STORAGE_EXTERN_GLOBAL 5
#define STORAGE_STATICGLOBAL 6
#define STORAGE_EXTERN_CONST 7
#define STORAGE_CONST 8
#define STORAGE_EXTERN_GLOBAL_CONST 9
#define STORAGE_GLOBAL_CONST 10




/* Define pointer levels for all types of pointers. It is infinite or the max range of unsigned long int available */
#define POINTERLEVELS  4294967295 UL


constexpr std::size_t SECTION_DATA = 1;
constexpr std::size_t SECTION_BSS = 2;
constexpr std::size_t SECTION_CONST = 3;

/* Symbol Entry class */
class SymbolEntry
{
private:

    std::size_t Type;
    std::size_t Size;
    std::size_t MemoryAddress;
    std::size_t StorageClass;
    std::size_t Section;
    std::size_t Reserved1;
    SCHAR8* name;
    Variant Data;
    BOOL isConst;
    BOOL isStatic;
    std::size_t linenumber;
    std::size_t columnnumber;
public:
    class SymbolEntry *pNext; /* create list of symbol bucket entries */
    /* Constructor */
    SymbolEntry();
    /* Destructor */
    ~SymbolEntry();
    /* Get Type */
    std::size_t GetType(void) const;
    /* Set Type */
    void SetType(std::size_t DataType);
    /* Get data */
    Variant GetData(void) const;
    /* Set data */
    void  SetData(Variant SymbolData);
    /* get char string symbol name*/
    SCHAR8* GetName(void) const;
    /* Set symbol name*/
    void SetName(SCHAR8* str);
    /* Get storage class */
    std::size_t GetStorageClass(void) const;
    /* Set storage class */
    void SetStorageClass(std::size_t SymbolStorage);
    /* Get size */
    std::size_t GetSize(void) const;
    /* Set size */
    void SetSize(std::size_t SymbolSize);
    /* Get Memory address */
    std::size_t GetMemoryAddress(void) const;
    /* Set Memory address */
    void SetMemoryAddress(std::size_t SymbolAddress);
    /* Get Section name */
    std::size_t GetSection(void) const;
    /* Set section */
    void SetSection(std::size_t SymbolSection);
    /* Delete memory allocated */
    void DeleteData(void);
    /* Is static */
    BOOL isSymbolStatic(void) const;
    /* Set a ident is static */
    void SetStatic(BOOL value);
    /* Is it a const variable */
    BOOL isConstant(void) const;
    /* Set const or not */
    void SetConst(BOOL value);

};

/*
A composite data type to get symbolvalue
*/
struct SymbolData
{
    BOOL isFound;
    SymbolEntry *pEntry;
};


/* New class definition of a symbol table for dynamic array of scopes */
class SymTable
{
private:
    std::size_t Scope;
    /* Use this to introduce breaks between symbol tables of different scopes? */
    /* We will have this feature for now */
    BOOL isTableFull;
    /* Check if it has static members */
    BOOL hasStatic;
    /* create a hash table. */
    SymbolEntry *SymbolEntryTable[MINIHASHTABLESIZE];
public:
    /* Constructor */
    SymTable();
    /* Destructor */
    ~SymTable();
    /* Set scope */
    void SetScope(std::size_t scope);
    /* Set scope */
    std::size_t GetScope(void) const;
    /* Set is empty or not */
    void SetTableStatus(bool Status);
    /* Table status */
    BOOL getTableStatus(void) const;
    /* Configure scope to have static variables */
    BOOL ContainsStaticMember(void) const;
    /* Set symbol table to have static member */
    void SetContainsStaticMember(void);
    /* Get Symbol entry */
    SymbolEntry* GetFirstSymbolEntry(std::size_t index) const;
    /* Set Symbol Entry */
    void SetFirstSymbolEntry(std::size_t index, SymbolEntry *pSymbolEntry);
};


/* Create a data structure for list of scopes with dynamic arrays.
Each scope is a hash table.
Start with a size of 50, increase size if number of symbols are more.
Measure perfomance of this DS.
Implement this with single/double linked list and dynamic array.
*/
class Scopes
{
private:
    /* This sets level of scopes */
    std::size_t Size;
    SymTable** ScopeLevel;
    SymTable** MoreScopes;
    BOOL hasStaticMember;
    /* Level of nested scopes */
    std::size_t CurrentLevel;
    /* This can be name of a function or _global*/
    SCHAR8* ScopeName;
public:
    /* Get size */
    std::size_t GetCurrentLevel(void) const;
    /* Set Size */
    void SetCurrentLevel(std::size_t value);
    /* Resize scope array */
    void Resize(void);
    /* Has sttaic member or not */
    BOOL ContainsStaticMember(void) const;
    /* Set static member */
    void SethasStaticMember(void);
    /* Get Symbol Table and set at a index.*/
    SymTable* GetSymbolTable(std::size_t index);
    void SetSymbolTable(std::size_t index, SymTable *Table);
    void UpdateSize(std::size_t Value);
    std::size_t GetScopeSize(void) const;
    /* get scope name*/
    SCHAR8* GetName(void) const;
    /* Set Scope name */
    void SetName(SCHAR8* str);
    /* Constructor */
    Scopes();
    /* Destructor */
    ~Scopes();
    /* Component Function: SymbolData GetSymbolData(SCHAR8* str)
    Arguments:  Symbol to be looked up, scope
    Returns:
    Description: This function  looks up a string in a hash table of linked lists with each linked list starting
    at each position of array element.
    Returns: Found or not found.
    Version : 0.1
    Notes:
    Execution may be slow due to nested loop.
    Lookup should get recent value of that symbol.
    Return a struct with a flag and the symbol entry containing value - a string or a numeric type?
    Change the return type to pointer?
    Design - Should this function return value of a symbol or a flat that its present or not?
    Check performance with both.
    */
    SymbolData GetSymbolData(SCHAR8* str);

    /* Component Function: BOOL LookupSymbolName(SCHAR8* str)
    Arguments:  Symbol to be looked up, scope
    Returns:
    Description: This function  looks up a string in a hash table of linked lists with each linked list starting
    at each position of array element.
    Returns: Found or not found.
    Version : 0.1
    Notes:
    */
    BOOL LookupSymbolName(SCHAR8* str);


    /* Component Function: void UpdateSymbol(SCHAR8* str, Variant& Data, std::size_t& Type,std::size_t Level, std::size_t& Section,std::size_t& DataSize, std::size_t& Address, std::size_t& storage, BOOL isSymbolStatic, BOOL isSymbolConst);
     Arguments:  Symbol to be looked up, scope    and other argument
     Returns:
     Description: This function  looks up a symbol by its char string name in a hash table and updates its data.
     at each position of array element.
     Returns: Found or not found.
     Version : 0.1
     Notes:
     Execution may be slow due to nested loop.
     Lookup should get recent value of that symbol.
      */
    void UpdateSymbol(SCHAR8* str, Variant& Data, std::size_t& Type, std::size_t Level, std::size_t& Section, std::size_t& DataSize, std::size_t& Address, std::size_t& storage, BOOL isSymbolStatic, BOOL isSymbolConst);

    /*  Component Function: void Scopes::UpdateSymbol(SCHAR8* str, Variant Data)
        Arguments:  Symbol to be looked up, scope
        Returns:
        Description: This function  looks up a symbol by its char string name in a hash table and updates its data.
        at each position of array element.
        Returns: Found or not found.
        Version : 0.1
        Notes:
        Execution may be slow due to nested loop.
        Lookup should get recent value of that symbol.
    */
    void UpdateSymbol(SCHAR8* str, Variant Data);

    /*
     Member Function: void InsertSymbolName(SCHAR8* str);
     Arguments:  Symbol name to be inserted
     Returns:
     Description: This function inserts a string into a hash table.
     Returns: None
     Version : 0.1  Initial version.
    		   0.2  Changed pTemp pointer initialization with    () function.
     Notes:
    */
    void InsertSymbolName(SCHAR8* str);
    /*
    Member Function: void InsertSymbol(SCHAR8* str, Variant &Data, std::size_t SymbolType, std::size_t SymbolSize, std::size_t  SymbolAddress, std::size_t SymbolStorage, std::size_t SymbolSection, BOOL isSymbolStatic, BOOL isSymbolConst)
     Arguments:  Symbol to be inserted, its data.
     Returns:
     Description: This function inserts a string into a hash table.
     Returns: None
     Version : 0.1  Initial version.
    		   0.2  Changed pTemp pointer initialization with    () function.
     Notes:
    */
    void InsertSymbol(SCHAR8* str, Variant &Data, std::size_t SymbolType, std::size_t SymbolSize, std::size_t  SymbolAddress, std::size_t SymbolStorage, std::size_t SymbolSection, BOOL isSymbolStatic, BOOL isSymbolConst);



    /*
     Component Function: void DeleteSymbol(SCHAR8* str)
     Arguments:  Symbol to be deleted in Current scope
     Returns:
     Description: This function  looks up a string in a hash table of linked lists with each linked list starting
     at each position of array element. The symbols start at second element of each array element.
     Returns: None
     Version : 0.1
     Notes:
     This function should look up ymbols stored in current scope.
     Modify this to look up symbols from current scope to level 1?
    */
    void DeleteSymbol(SCHAR8* str);

    /*
    Delete an entire scope in a level.
    */
    void Delete(std::size_t Level);
    /*
    Delete an entire scope in a level.
    */
    void EraseAllData(void);


    /*
     Component Function: void EnterLocalScope(void)
     Arguments:  Number to be hashed
     Description: This function creates a local scope and makes it current scope.
     Returns: None
     Version : 0.1
    Notes:
     */
    void EnterLocalScope(void);

    /*
     Component Function: void ExitLocalScope(Scopes* CurrentScope)
     Arguments:  Number to be hashed
     Description: This function exits currentscope
     Returns: None
     Version : 0.1
    Notes:
     */
    void ExitLocalScope(void);

    /*
     Component Function: void Scopes::EnterFileScope(void)
     Arguments:  Number to be hashed
     Description: This function creates File scope and block scopes in global scope which is outside of functions.
     Returns: None
     Version : 0.1
     Notes:
     When entering a scope, a new symbol table is to be created for that scope.
     This function is called only for scopes in functions.
     Function being defined.
     This function may not be necesary
    */
    void EnterFileScope(void);
};



/* Create new symbol table */
inline SymTable* CreateNewSymbolTable(void);
/* Create new symbol entry */
inline SymbolEntry* CreateNewSymbolEntry(void);
/*
 Component std::size_t strlength(const SCHAR8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(const SCHAR8 *str);

/*
 Component std::size_t strlength(SCHAR8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(SCHAR8 *str);

/*
 Component Function: void SetSymbolData(SymbolEntry *pNode, SCHAR8* str, Variant &Data, std::size_t Type, std::size_t Size, std::size_t Address, std::size_t storage, std::size_t Section,BOOL isSymbolStatic, BOOL isSymbolConst)
 Arguments:  None
 Returns:
 Description: This function prints all elements of symbol tables.
 Returns: None
 Version : 0.1
 Notes:
This function Sets symbol Data
*/
void SetSymbolData(SymbolEntry *pNode, SCHAR8* str, Variant &Data, std::size_t Type, std::size_t Size, std::size_t Address, std::size_t storage, std::size_t Section, BOOL isSymbolStatic, BOOL isSymbolConst);

/*
 Component Function: UCHAR8 getStrChar(std::size_t digit)
 Arguments:  None
 returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
UCHAR8 getStrChar(const std::size_t& digit);

/* Function doing a single memcopy from one string to another */
void Memcopy(SCHAR8*& dst, SCHAR8* src);


/* Function doing a single memcopy from one string to another */
void Memcopy(SCHAR8*& dst, const SCHAR8* src);

#endif
