/*
Name :  Symboltable.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module contains symbol table class and its functions.
Notes:
*/



/*


Coding Log:
09-09-19- Symbol table project taken as baseline. project compiles under iSO C++ 11.
10-09-19  Created IO module. Added GetInput() and diplayOutput() functions. Changed getInput() function. Created SCHAR8* getInput() function
          Added data structures module for numeric functions and operator look up. Changed DataStructures::DetectOpType Function
          Changed Datastructures module. Trucated the module to a few functions relevant.
11-09-19  Created parseCharString function. Created SCHAR8 PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index).Created IsOperChar() function in DataStructures module.
12-09-19  Created TokenProcessor() function, created getTokenCharstring() function,created getNextToken() function.
13-09-19  Changed dataStructures and lexer namespaces. Edited getTokencharstring() function.
16-09-19  Changed Stack module. Changed Functions in lexer module. Edited Tokenprocessor Module. Changed Scopes Class and member functions in Symbol table module.
          Created insertname and Lookupname functions in Scopes Class. Change MemCopy() function in SymbolTable class
          Changed Lexer::getTokencharstring(void) function to check for end of str when adding newline.

22-09-19  Changed getTokencharstring function to detect oeprations as a=!b a=~a and a=!a and a+=b, a-=b, a/=b, a*=b.  Added constants in Datastructures.h
          Added some more token error detection.
25-09-19  Added tree,arithmetic, op, postfix stack module form arithmeitc exprsssion evalulator.  Add arithmetic and shift operators. Added unary minus and unaruy plus fuctionality.
          Added DetectOpToken() function.
29-09-19 Created a stack for token processor . Corrected a error in TokenProcessor() for  detection of = operator.
01-10-19 Changed Parse Tokenstream() function. Added OPEQUALTO operator.Function being edited.
07-10-19 Changed Lexer::getTokencharstring(void) function. Created more features for ident detection.
         Added error detection.
10-10-19 Merged binary expression tree functionality from Arithmetic expression evaluator into this project.
         Modified InsertNode, PrintNode functions in Binary tree namespace.
		 Added new operators and changed node data in void  Parser::ConstructBinaryExpressionTree(void) function.
		 Added error detection for missing ( and ) tokens.
		 Added UpdateSymbol() function in Scopes:: class Removed static, const and other parameters of every symbol.
		 The current scope which is at file scope level will contain symbols or identifiers which contain empty values or a double value.
11-10-19 Created test cases for Computearithmetic(), evaluate expressiontree()
13-10-19 Created testcase for DeleteTree() function.Added equaltoopdetected flag in Testevaluateexpresssion() function.
14-10-19 Changed ComputeArithmetic() function.
16-10-19 Changed Scopes::LookupSymbol() function into Scopes::GetSymbolData() function.
17-10-19 Added error detection in Tree module. Moved arithmetic syntax error detection from lexer to parser/tree module.
20-10-19
22-10-19 Changed ComputeArithmetic() function in Tree module. Created LookupNameinSymbolTable() in parser module.
23-10-19 Added code for memory allcation error detection SymbolTable and Parser.
25-10-19 Modified and cleaned ComputeAritmetic() function in Parser module .Corrected a bug in parser which detected precedence of = and ( incorrectly.
26-10-19 Added eraseAllData() function for erasing data of identifers in symbol take in Scopes:: namespace
28-10-19 Added unary minus in parser module. Added code for unary minus and unary plus in Tree functions.
31-10-19 Added some features for unary plus operator.Implemented unary plus . Added featues for unary plus in Lexer::DetectOpToken() and DetectOpString() functions
         in lexer module.
*/

#include "Symboltable.h"
#include <iostream>


/* constructor for symbol Entry */
SymbolEntry::SymbolEntry()
{
    Type = NOTADATATYPE;
    name = nullptr;
    Size = 0;
    MemoryAddress = 0;
    StorageClass = 0;
    Reserved1 = 0;
    Section = 0;
    Reserved1 = 0;
    isConst = false;
    isStatic = false;
    pNext = nullptr;
    Data.u.cString = nullptr;
    linenumber=0;
    columnnumber=0;
}

/* Destructor for symbol Entry */
SymbolEntry::~SymbolEntry()
{
    if (!isStatic)
    {
        /* Delete all memory allocated */
        if (name)
        {
            delete[] name;
            name = nullptr;
        }
        /* Edit this section */
        /* Code to be added for c string*/
        if (Data.u.cString)
        {
            //char *ptr = Data.u.cString;
            //delete[] ptr;
            delete[] Data.u.cString;
            //ptr = nullptr;
            Data.u.cString=nullptr;
        }
    }
}
void SymbolEntry::SetType(std::size_t SymbolType)
{
    Type = SymbolType;
}

std::size_t SymbolEntry::GetType(void) const
{
    return Type;
}


void SymbolEntry::SetConst(BOOL value)
{
    isConst = value;
}

BOOL SymbolEntry::isConstant(void) const
{
    return isConst;
}


/* Set data */
void  SymbolEntry::SetData(Variant SymbolData)
{
    Data = SymbolData;
}
/* Get data */
Variant SymbolEntry::GetData(void) const
{
    return Data;
}

/* get symbol name*/
SCHAR8* SymbolEntry::GetName(void) const
{
    return name;
}

/* Set symbol name*/
/* Code edit here */
void SymbolEntry::SetName(SCHAR8* str)
{
    /* Create char string using memcpy with a SCHAR8* string */
    /* Check size of str in a external function */
    Memcopy(name,str);
}

/* Get storage class */
std::size_t SymbolEntry::GetStorageClass(void) const
{
    return StorageClass;
}
/* Set storage class */
void SymbolEntry::SetStorageClass(std::size_t SymbolStorage)
{
    StorageClass = SymbolStorage;

}

/* Get size */
std::size_t SymbolEntry::GetSize() const
{
    return Size;
}
/* Set size */
void SymbolEntry::SetSize(std::size_t size)
{
    Size = size;
}

/* Get Memory address */
std::size_t SymbolEntry::GetMemoryAddress(void) const
{
    return MemoryAddress;
}
/* Set Memory address */
void SymbolEntry::SetMemoryAddress(std::size_t SymbolAddress)
{
    MemoryAddress = SymbolAddress;
}

/* Get Section name */
std::size_t SymbolEntry::GetSection(void) const
{
    return Section;
}
/* Set section */
void SymbolEntry::SetSection(std::size_t SymbolSection)
{
    Section = SymbolSection;
}



/* Check symbol if its static  */
BOOL SymbolEntry::isSymbolStatic(void) const
{
    return isStatic;
}
/* Set section */
void SymbolEntry::SetStatic(BOOL value)
{
    isStatic = value;
}


/* Delete data in SymbolEntry */
void SymbolEntry::DeleteData(void)
{
    /* Keep name in symbol and delete the rest. */
    Data.isDataInitialized=false;
    Data.Type=NOTADATATYPE;
    Data.u.doubleValue=0;
}



/* Constructor for symbol Table */
SymTable::SymTable()
{
    Scope = 0;
    /* Use this to introduce breaks between symbol tables of different functions*/
    isTableFull = false;

    for (std::size_t index = 0; index < MINIHASHTABLESIZE; index++)
    {
        SymbolEntryTable[index] = nullptr;
    }


}

/* Destructor for symbol Table */
SymTable::~SymTable()
{
    for (std::size_t index = 0; index < MINIHASHTABLESIZE; index++)
    {
        /* Delete all bucket lists */
    }
    /*Reorganize each bucket properly after deletion. */
}

/* Set scope in Symbol Table*/
void SymTable::SetScope(std::size_t scope)
{
    Scope = scope;
}

/* Set scope */
std::size_t SymTable::GetScope(void) const
{
    return Scope;
}

/* Check if symbol table has static member */
BOOL SymTable::ContainsStaticMember(void) const
{
    return hasStatic;
}
/* Set that table has static member */
void SymTable::SetContainsStaticMember(void)
{
    hasStatic = true;
}
/* Set hashTable is empty or not */
void SymTable::SetTableStatus(BOOL Status)
{
    isTableFull = Status;
}
/* Table status */
BOOL SymTable::getTableStatus(void) const
{
    return isTableFull;
}


/* Member functions of symbolTable class */
void SymTable::SetFirstSymbolEntry(std::size_t index, SymbolEntry *pSymbolEntry)
{
    SymbolEntryTable[index] = pSymbolEntry;
}


SymbolEntry* SymTable::GetFirstSymbolEntry(std::size_t index) const
{
    return SymbolEntryTable[index];
}





/* Constructors for scopes */
Scopes::Scopes()
{
    this->ScopeLevel = nullptr;
    this->CurrentLevel = 0;
    this->hasStaticMember = false;
    ScopeName = nullptr;
    this->Size = 0;
    this->ScopeLevel = new SymTable*[50];
    if (this->ScopeLevel)
    {
        for (std::size_t i = 0; i < 50; i++)
        {
            this->ScopeLevel[i] = nullptr;
        }
    }
    else
    {
        ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
    }
}

/* Destructors for scopes.
Being designed. */
Scopes::~Scopes()
{

}

/* Resize Scope array */

/* Change this function.*/
void Scopes::Resize(void)
{
    MoreScopes = new SymTable*[CurrentLevel + 50];
    if (MoreScopes)
    {
        for (std::size_t i = 0; i < CurrentLevel; i++)
        {
            MoreScopes[i] = ScopeLevel[i];
        }
        delete[] ScopeLevel;
        ScopeLevel = MoreScopes;
    }
    else
    {
        ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
    }
}

/* Set static member */
void Scopes::SethasStaticMember(void)
{
    hasStaticMember = true;
}

/* Get static member */
BOOL Scopes::ContainsStaticMember(void) const
{
    return hasStaticMember;
}
/* Get size */
std::size_t Scopes::GetCurrentLevel(void) const
{
    return CurrentLevel;
}

/* Get size */
void Scopes::SetCurrentLevel(std::size_t value)
{
    CurrentLevel = value;
}


/* Functions to configure SymbolTable */
void Scopes::SetSymbolTable(std::size_t index, SymTable *Table)
{
    ScopeLevel[index] = Table;
}
/* Function to get Symbol Table */
SymTable* Scopes::GetSymbolTable(std::size_t index)
{
    return ScopeLevel[index];
}



/* Set major scope name*/
void Scopes::SetName(SCHAR8* str)
{
    /* Create char string using memcpy with a SCHAR8* string */
    /* Check size of str in a external function */
    if (ScopeName)
    {
        delete[] ScopeName;
        ScopeName = nullptr;
    }
    std::size_t Length = strlength(str);
    std::size_t index = 0;
    ScopeName = new SCHAR8[Length + 1];
    for (index = 0; index < Length; index++)
    {
        ScopeName[index] = str[index];
    }
    ScopeName[index] = '\0';
}

/* Get Scope name */
SCHAR8* Scopes::GetName() const
{
    return ScopeName;
}


/* Get size and set size */
void Scopes::UpdateSize(std::size_t value)
{
    Size = value;
}

std::size_t Scopes::GetScopeSize(void) const
{
    return Size;
}

/* Component Function:  SymbolData Scopes::GetSymbolData(SCHAR8* str)
 Arguments:  Symbol to be looked up, scope
 Returns:
 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
 at each position of array element.
 Returns: Found or not found.
 Version : 0.1
 Notes:
 Execution may be slow due to nested loop.
 Lookup should get recent value of that symbol.
 Return a struct with a flag and the symbol entry containing value - a string or a numeric type?
 Change the return type to pointer?
 Design - Should this function return value of a symbol or a flat that its present or not?
 Check performance with both.


 This function is looking up symbol only in current level during construction of symbol table.
 Change the function so that it looks up symbols upto Size of each Scope. Current level increases and decreases
 during construction of each scope but size will remain constant as it sthe maximum number of scopes which were
 ever constructed.
 --
 --
 --
 --
 --




 This function can be used to check lookup of definition of extern variables in other source files.
 */
SymbolData Scopes::GetSymbolData(SCHAR8* str)
{
    BOOL SymbolFound = false;
    std::size_t Symbolindex = StringHash(str);
    std::size_t level = this->GetCurrentLevel();
    SymbolEntry* pCurrentSymbolEntry = nullptr;
    SymbolData SymData { false, nullptr };

    /* Lookup symbol in scopes n to 1 */
    for (SINT32 index = level; index >= 0 && SymbolFound == false; index--)
    {
        SymTable *pCurrentTable = this->GetSymbolTable(index); /* One element lookup */
        if (pCurrentTable)
        {
            pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(Symbolindex);
            while ((pCurrentSymbolEntry) && (!SymbolFound))
            {
                if (strcmp(str, pCurrentSymbolEntry->GetName()) ==0)
                {
                    SymbolFound = true;
                    SymData.isFound=true;
                    SymData.pEntry= pCurrentSymbolEntry;
                }
                if (!SymbolFound)
                {
                    pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
                }
            }
        }
    }
    return SymData;
}

/*
 Component Function: SymbolData Scopes::LookupSymbolName(SCHAR8* str)
 Arguments:  Symbol to be looked up, scope
 Returns:
 Description: This function  looks up a string in a hash table of linked lists with each linked list starting
 at each position of array element.
 Returns: Found or not found.
 Version : 0.1
 Notes:
 returns true if found or else false.
 */
BOOL Scopes::LookupSymbolName(SCHAR8* str)
{
    BOOL SymbolFound = false;
    std::size_t Symbolindex = StringHash(str);
    std::size_t level = this->GetCurrentLevel();
    SymbolEntry* pCurrentSymbolEntry = nullptr;
    /* Lookup symbol in scopes n to 1 */
    for (SINT32 index = level; index >= 0 && SymbolFound == false; index--)
    {
        SymTable *pCurrentTable = this->GetSymbolTable(index); /* One element lookup */
        if (pCurrentTable)
        {
            pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(Symbolindex);
            while ((pCurrentSymbolEntry) && (!SymbolFound))
            {
                if (strcmp(str, pCurrentSymbolEntry->GetName()) ==0)
                {
                    SymbolFound = true;
                }

                if (!SymbolFound)
                {
                    pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
                }
            }
        }
    }
    return SymbolFound;
}

/* Component Function: void UpdateSymbol(SCHAR8* str, Variant& Data, std::size_t& Type,std::size_t Level, std::size_t& Section,std::size_t& DataSize, std::size_t& Address, std::size_t& storage,, BOOL isSymbolStatic, BOOL isSymbolConst);
 Arguments:  Symbol to be looked up, scope    and other argument
 Returns:
 Description: This function  looks up a symbol by its char string name in a hash table and updates its data.
 at each position of array element.
 Returns: Found or not found.
 Version : 0.1
 Notes:
 Execution may be slow due to nested loop.
 Lookup should get recent value of that symbol.
  */
void Scopes::UpdateSymbol(SCHAR8* str, Variant& Data, std::size_t& Type, std::size_t Level, std::size_t& Section, std::size_t& DataSize, std::size_t& Address, std::size_t& storage, BOOL isSymbolStatic, BOOL isSymbolConst)
{
    BOOL SymbolUpdated = false;
    std::size_t Symbolindex = StringHash(str);
    SymbolEntry* pCurrentSymbolEntry = nullptr;
    /* Lookup symbol in scopes n to 1 */
    SymTable *pCurrentTable = this->GetSymbolTable(Level); /* One element lookup */
    if (pCurrentTable)
    {
        pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(Symbolindex);
        while ((pCurrentSymbolEntry) && (!SymbolUpdated))
        {
            if (!strcmp(str, pCurrentSymbolEntry->GetName()))
            {
                /* Set variant data here */
                SetSymbolData(pCurrentSymbolEntry, str, Data, Type, DataSize, Address, storage, Section, isSymbolStatic, isSymbolConst);
                SymbolUpdated = true;
                continue;
            }
            if (!SymbolUpdated)
            {
                pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
            }
        }
    }
}


/* Component Function: void Scopes::UpdateSymbol(SCHAR8* str, Variant Data)
 Arguments:  Symbol to be looked up, scope
 Returns:
 Description: This function  looks up a symbol by its char string name in a hash table and updates its data.
 at each position of array element.
 Returns: Found or not found.
 Version : 0.1
 Notes:
 Lookup should get recent value of that symbol.
 Test if symbol is actually updated.

  */
void Scopes::UpdateSymbol(SCHAR8* str, Variant Data)
{
    BOOL SymbolUpdated = false;
    std::size_t Symbolindex = StringHash(str);
    SymbolEntry* pCurrentSymbolEntry = nullptr;
    /* Lookup symbol in scopes n to 1 */
    SymTable *pCurrentTable = this->GetSymbolTable(this->CurrentLevel); /* One element lookup */
    if (pCurrentTable)
    {
        pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(Symbolindex);
        /* There may be many symbol entries which match for first entry */
        while ((pCurrentSymbolEntry) && (!SymbolUpdated))
        {
            if (strcmp(str, pCurrentSymbolEntry->GetName()) ==0)
            {
                /* Set variant data here */
                pCurrentSymbolEntry->SetData(Data);
                SymbolUpdated = true;
                //std::cout << "Symbol updated" << std::endl;
            }
            if (!SymbolUpdated)
            {
                pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
            }
        }
    }
}



/*
 Component Function: void Scopes::InsertSymbol(SCHAR8* str, Variant &Data, std::size_t SymbolType, std::size_t SymbolSize, std::size_t  SymbolAddress, std::size_t SymbolStorage, std::size_t SymbolSection, BOOL isSymbolStatic, BOOL isSymbolConst)
 Arguments:  Symbol to be inserted, its data.
 Returns:
 Description: This function inserts a string into a hash table.
 Returns: None
 Version : 0.1  Initial version.
		   0.2  Changed pTemp pointer initialization with SetSymbolData() function.
		   0.3 Changed Symboltable and SCope class objects to enable hasStatic flag while inserting static symbols.
 Notes:
*/
void Scopes::InsertSymbol(SCHAR8* str, Variant &Data, std::size_t SymbolType, std::size_t SymbolSize, std::size_t  SymbolAddress, std::size_t SymbolStorage, std::size_t SymbolSection, BOOL isSymbolStatic, BOOL isSymbolConst)
{
    std::size_t SymbolEntryindex = StringHash(str);
    SymbolEntry *pCurrentSymbolEntry = nullptr;
    SymTable *pCurrentTable = this->GetSymbolTable(this->GetCurrentLevel());
    //std::cout << "string to be inserted  " << str << std::endl;
    if (!pCurrentTable)
    {
        pCurrentTable = CreateNewSymbolTable();
        this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
    }
    pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(SymbolEntryindex);
    //std::cout << "at Middle of InsertSymbol function " << std::endl;
    /* First element in bucket is empty */
    if (pCurrentSymbolEntry == nullptr)
    {
        pCurrentSymbolEntry = CreateNewSymbolEntry();
        if (pCurrentSymbolEntry)
        {
            //std::cout << "Created symbol entry node " << std::endl;
            //system("pause");
            SetSymbolData(pCurrentSymbolEntry, str, Data, SymbolType, SymbolSize, SymbolAddress, SymbolStorage, SymbolSection, isSymbolStatic, isSymbolConst);
            pCurrentTable->SetFirstSymbolEntry(SymbolEntryindex, pCurrentSymbolEntry);
            if (isSymbolStatic)
            {
                if (!pCurrentTable->ContainsStaticMember())
                {
                    pCurrentTable->SetContainsStaticMember();
                }
                if (!this->ContainsStaticMember())
                {
                    this->SethasStaticMember();
                }
            }
            this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
            //std::cout << "Inserted " << pCurrentSymbolEntry->GetName() << std::endl;
            //std::cout << "Size in InsertSymbol " << pCurrentSymbolEntry->GetSize() << std::endl;
        }
        else
        {
            ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
        }
    }
    else
    {
        SymbolEntry *pTemp = CreateNewSymbolEntry();
        if (pTemp)
        {
            //std::cout << "Created symbol entry node " << std::endl;
            /* Insert a node next to first element of bucket list */
            SetSymbolData(pTemp, str, Data, SymbolType, SymbolSize, SymbolAddress, SymbolStorage, SymbolSection, isSymbolStatic, isSymbolConst);
            pTemp->pNext = pCurrentSymbolEntry;
            pCurrentTable->SetFirstSymbolEntry(SymbolEntryindex, pTemp);
            if (isSymbolStatic)
            {
                if (!pCurrentTable->ContainsStaticMember())
                {
                    pCurrentTable->SetContainsStaticMember();
                }
                if (!this->ContainsStaticMember())
                {
                    this->SethasStaticMember();
                }
            }
            this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
            //std::cout << "Inserted " << pTemp->GetName() << std::endl;
        }
        else
        {
            ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
        }
    }
}



/*
 Component Function: void Scopes::InsertSymbolName(SCHAR8* str)
 Arguments:  Symbol name to be inserted
 Returns:
 Description: This function inserts a string into a hash table.
 Returns: None
 Version : 0.1  Initial version.
		   0.2  Changed pTemp pointer initialization with SetSymbolData() function.
		   0.3 Changed Symboltable and SCope class objects to enable hasStatic flag while inserting static symbols.
 Notes:
*/
void Scopes::InsertSymbolName(SCHAR8* str)
{
    std::size_t SymbolEntryindex = StringHash(str);
    SymbolEntry *pCurrentSymbolEntry = nullptr;
    SymTable *pCurrentTable = this->GetSymbolTable(this->GetCurrentLevel());
    //std::cout << "string to be inserted  " << str << std::endl;
    if (!pCurrentTable)
    {
        pCurrentTable = CreateNewSymbolTable();
        this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
    }
    pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(SymbolEntryindex);
    //std::cout << "at Middle of InsertSymbol function " << std::endl;
    /* First element in bucket is empty */
    if (pCurrentSymbolEntry == nullptr)
    {
        pCurrentSymbolEntry = CreateNewSymbolEntry();
        
        if (pCurrentSymbolEntry)
        {
            //std::cout << "Created symbol entry node " << std::endl;
            //system("pause");
            //SetSymbolData(pCurrentSymbolEntry, str, Data, SymbolType, SymbolSize, SymbolAddress, SymbolStorage, SymbolSection, isSymbolStatic, isSymbolConst);
            pCurrentSymbolEntry->SetName(str);
            pCurrentTable->SetFirstSymbolEntry(SymbolEntryindex, pCurrentSymbolEntry);
            this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
            //std::cout << "Inserted " << pCurrentSymbolEntry->GetName() << std::endl;
            //std::cout << "Size in InsertSymbol " << pCurrentSymbolEntry->GetSize() << std::endl;
        }
        else
        {
            ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
        }
    }
    else
    {
        SymbolEntry *pTemp = CreateNewSymbolEntry();
        if (pTemp)
        {
            //std::cout << "Created symbol entry node " << std::endl;
            /* Insert a node next to first element of bucket list */
            pTemp->SetName(str);
            pTemp->pNext = pCurrentSymbolEntry;
            pCurrentTable->SetFirstSymbolEntry(SymbolEntryindex, pTemp);
            this->SetSymbolTable(this->GetCurrentLevel(), pCurrentTable);
            //std::cout << "Inserted " << pTemp->GetName() << std::endl;
        }
        else
        {
            ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
        }
    }
}

/*
 Component Function: void DeleteSymbol(SCHAR8* str)
 Arguments:  Symbol to be deleted and its scope
 Returns:
 Description: This function deletes a symbol
 Returns: None
 Version : 0.1
 Notes:
 This function should look up symbols stored in scope and delete them.
 Modify this to look up symbols from current scope to level 1 and delete them?
 Check this function for static symbol deletion.

*/
void Scopes::DeleteSymbol(SCHAR8* str)
{
    BOOL SymbolDeleted = false;
    std::size_t SymbolEntryindex = StringHash(str);
    /* using a fuction to get pointer instead of array index */
    SymTable *pCurrentTable = this->GetSymbolTable(this->GetCurrentLevel()); /* One element lookup */
    SymbolEntry *pCurrentSymbolEntry = nullptr;
    if (pCurrentTable)
    {
        /* First element is head of bucket list */
        pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(SymbolEntryindex);
        /* pCurrentSymbolEntry is Head */
        SymbolEntry *pTemp = nullptr;
        if (pCurrentSymbolEntry)
        {
            /* First element in bucket list is the entry */
            if (!strcmp(pCurrentSymbolEntry->GetName(), str))
            {
                if (!pCurrentSymbolEntry->isSymbolStatic())
                {
                    /* This list resizes itself */
                    pTemp = pCurrentSymbolEntry->pNext;
                    delete pCurrentSymbolEntry;
                    pCurrentSymbolEntry = pTemp;
                    pCurrentTable->SetFirstSymbolEntry(SymbolEntryindex, pTemp);
                    SymbolDeleted = true;
                }

            }
            else
            {
                /* Entry is somewhere in bucket list */
                /* After deleting the entry resizes itself */
                while (pCurrentSymbolEntry->pNext && (!SymbolDeleted))
                {
                    if (!pCurrentSymbolEntry->pNext->isSymbolStatic())
                    {
                        if (!strcmp(str, pCurrentSymbolEntry->pNext->GetName()))
                        {
                            /* Delete symbol entry */
                            pTemp = pCurrentSymbolEntry->pNext;
                            pCurrentSymbolEntry->pNext = pCurrentSymbolEntry->pNext->pNext;
                            if (pTemp)
                            {
                                delete pTemp;
                                pTemp = nullptr;
                            }
                            SymbolDeleted = true;
                            continue;
                        }
                    }
                    if (!SymbolDeleted)
                    {
                        pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
                    }
                }
            }
        }
    }
}



/*
 Component Function: void Scopes::Delete(std::size_t Level)
 Arguments:  Delete a symbol table in a level.
 Returns:
 Description: This function deletes a symbol table at a level.
 Returns: None
 Version : 0.1
 Notes:
 This is not persisitent delete function which looks up symbols stored in a scope and delete them.
 Implement this function function which deletes only symbol tables which dont have static symbols.if static symbols are present,
 don't delete that symbol table.
 Implement a function for final deletion which includes static variables when parser is done with a source file.
 */
void Scopes::Delete(std::size_t Level)
{
    SymTable *pCurrentTable = this->GetSymbolTable(Level); /* One element lookup */
    SymbolEntry *pCurrentSymbolEntry = nullptr, *pTemp = nullptr;
    if (pCurrentTable)
    {
        if (pCurrentTable->ContainsStaticMember())
        {
            for (std::size_t index = 0; index < MINIHASHTABLESIZE; index++)
            {
                pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(index);
                while (pCurrentSymbolEntry)
                {
                    pTemp = pCurrentSymbolEntry->pNext;
                    if (!pCurrentSymbolEntry->isSymbolStatic())
                    {
                        delete pCurrentSymbolEntry;
                        pCurrentSymbolEntry = nullptr;
                    }
                    pCurrentSymbolEntry = pTemp;
                }
            }
            /* Cannot delete entire symbol table as it contains a static member. */
        }
        else
        {
            for (std::size_t index = 0; index < MINIHASHTABLESIZE; index++)
            {
                pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(index);
                while (pCurrentSymbolEntry)
                {
                    pTemp = pCurrentSymbolEntry->pNext;
                    delete pCurrentSymbolEntry;
                    pCurrentSymbolEntry = nullptr;
                    pCurrentSymbolEntry = pTemp;
                }
            }
            /* Delete pCurrentTable if it doesnt contain static symbol.*/
            delete pCurrentTable;
            pCurrentTable = nullptr;
            this->SetSymbolTable(Level, pCurrentTable);
        }
    }
}



/*
 Component Function: void Scopes::EraseAllData(void)
 Arguments:  Delete a symbol table in a level.
 Returns:
 Description: This function Erases all data in a symbol table.
 Returns: None
 Version : 0.1
 Notes:
 This is not persisitent delete function which looks up symbols stored in a scope and delete them.
 Implement this function function which deletes only symbol tables which dont have static symbols.if static symbols are present,
 don't delete that symbol table.
 Implement a function for final deletion which includes static variables when parser is done with a source file.
 */
void Scopes::EraseAllData(void)
{
    SymTable *pCurrentTable = this->GetSymbolTable(this->CurrentLevel); /* One symbol table lookup*/
    SymbolEntry *pCurrentSymbolEntry = nullptr;
    Variant NullData;
    NullData.isDataInitialized=false;
    NullData.u.doubleValue=0;
    if (pCurrentTable)
    {
        for (std::size_t index = 0; index < MINIHASHTABLESIZE; index++)
        {
            pCurrentSymbolEntry = pCurrentTable->GetFirstSymbolEntry(index);
            while (pCurrentSymbolEntry)
            {
                pCurrentSymbolEntry->SetData(NullData);
                pCurrentSymbolEntry = pCurrentSymbolEntry->pNext;
            }
        }
    }
}
/*
 Component Function: void Scopes::EnterFileScope(void)
 Arguments:  Number to be hashed
 Description: This function creates a global scope
 Returns: None
 Version : 0.1
Notes:
When entering a scope, a new symbol table is to be created for that scope.
This function is called only for scopes in functions.
Function being defined.
This function may not be necesary.
 */
void Scopes::EnterFileScope(void)
{
    this->SetCurrentLevel(0);
    SymTable *pTemp = new SymTable;
    this->SetSymbolTable(this->GetCurrentLevel(), pTemp);
}

/*
 Component Function: void Scopes::EnterLocalScope(Scopes* CurrentScope)
 Arguments:  Number to be hashed
 Description: This function creates a local scope
 Returns: None
 Version : 0.1
Notes:
When entering a scope, a new symbol table is to be created for that scope.
This function is called only for scopes in functions.
Function being defined.

Adjust size which is number of scopes here?
*/
void Scopes::EnterLocalScope(void)
{
    CurrentLevel++;
    if (CurrentLevel % 50 == 0)
    {
        this->Resize();
    }
    SymTable *pTemp = new SymTable;
    this->SetSymbolTable(this->GetCurrentLevel(), pTemp);

}

/*
 Component Function: void Scopes::ExitLocalScope(Scopes* CurrentScope)
 Arguments:  Number to be hashed
 Description: This function creates a local scope
 Returns: None
 Version : 0.1
Notes:
at scope exit,  symbol table within that scope are to be deleted except those which contain static members.
Function being defined.

Adjust size which is number of scopes here?
 */
void Scopes::ExitLocalScope(void)
{
    if (!this->ContainsStaticMember())
    {
        this->Delete(this->GetCurrentLevel());
    }
    this->CurrentLevel--;
}

/* Function doing a single memcopy from one string to another. Check this function. */
void Memcopy(SCHAR8*& dst, SCHAR8* src)
{

    std::size_t Length = strlength(src);
    std::size_t index = 0;
    dst = new char[Length + 1];
    if (dst)
    {
        for (index = 0; index < Length; index++)
        {
            dst[index] = src[index];
        }
        dst[index] = '\0';

    }
    else
    {
        ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
    }


}
/* Function doing a single memcopy from one string to another */
void Memcopy(SCHAR8*& dst, const  SCHAR8* src)
{
    std::size_t Length = strlength(src);
    std::size_t index = 0;
    if (dst !=nullptr)
        delete[] dst;
    dst = new char[Length + 1];
    
    
    if (dst)
    {
        for (index = 0; index < Length; index++)
        {
            dst[index] = src[index];
        }
        dst[index] = '\0';
    }

    else
    {
        ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
    }
}


/*
 Component Function: void SetSymbolData(SymbolEntry *pNode, SCHAR8* str, Variant &Data, std::size_t Type, std::size_t Size, std::size_t Address, std::size_t storage, std::size_t Section,, BOOL isSymbolStatic, BOOL isSymbolConst)
 Arguments:  None
 Returns:
 Description: This function prints all elements of symbol tables.
 Returns: None
 Version : 0.1
 Notes:
This function Sets symbol Data
*/
void SetSymbolData(SymbolEntry *pNode, SCHAR8* str, Variant &Data, std::size_t Type, std::size_t Size, std::size_t Address, std::size_t storage, std::size_t Section, BOOL isSymbolStatic, BOOL isSymbolConst)
{
    pNode->SetData(Data);
    pNode->SetSize(Size);
    pNode->SetMemoryAddress(Address);
    pNode->SetStorageClass(storage);
    pNode->SetType(Type);
    pNode->SetName(str);
    pNode->SetSection(Section);
    pNode->SetStatic(isSymbolStatic);
    pNode->SetConst(isSymbolConst);
    pNode->pNext = nullptr;
}

/*
 Component std::size_t strlength(const SCHAR8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(const SCHAR8 *str)
{
    std::size_t count = 0;
    while (*str)
    {
        count++;
        str++;
    }
    return count;
}

/*
 Component std::size_t strlength(SCHAR8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(SCHAR8 *str)
{
    std::size_t count = 0;
    while (*str)
    {
        count++;
        str++;
    }
    return count;
}

/* Create new symbol table */
inline SymTable* CreateNewSymbolTable(void)
{
    //SymTable* pTemp = new SymTable;
    return new SymTable;
}

/* Create new symbol entry */
inline SymbolEntry* CreateNewSymbolEntry(void)
{
    return new SymbolEntry;
}


